// Code based on "How to use libJpeg in a C++ project"  (https://numberduck.com/Blog/?nPostId=2)
// Downloaded 2016-12-17
// Changes include, but are not limited to:
// - updating coding style to QRSolver project
// - always treating image as grayscale / 1 byte per pixel
// - adding Save() and SetImage() functions

#include <log.h>
#include "JpegConverter.h"

JpegConverter::JpegConverter() { myImageInfo = NULL; }

JpegConverter::~JpegConverter() { Cleanup(); }

const JpegConverter::ImageInfo* JpegConverter::SetImage(JpegConverter::ImageInfo* input) {
  if (myImageInfo == input) {
    return myImageInfo;
  }
  if (input == NULL) {
    return NULL;
  }
  Cleanup();

  myImageInfo = new ImageInfo();
  myImageInfo->width = input->width;
  myImageInfo->height = input->height;
  myImageInfo->numComponents = input->numComponents;

  unsigned int dataSize = myImageInfo->width * myImageInfo->height * myImageInfo->numComponents;
  myImageInfo->data = new uint8_t[dataSize];
  memcpy(myImageInfo->data, input->data, dataSize);

  return myImageInfo;
}

const JpegConverter::ImageInfo* JpegConverter::Load(const std::string& filename) {
  Cleanup();

  jpeg_decompress_struct cinfo;
  ErrorManager errorManager;

  FILE* pFile = fopen(filename.c_str(), "rb");
  if (!pFile) {
    return NULL;
  }

  // set our custom error handler
  cinfo.err = jpeg_std_error(&errorManager.defaultErrorManager);
  errorManager.defaultErrorManager.error_exit = ErrorExit;
  errorManager.defaultErrorManager.output_message = OutputMessage;
  if (setjmp(errorManager.jumpBuffer)) {
    // We jump here on errors
    Cleanup();
    jpeg_destroy_decompress(&cinfo);
    fclose(pFile);
    return NULL;
  }

  jpeg_create_decompress(&cinfo);
  jpeg_stdio_src(&cinfo, pFile);
  jpeg_read_header(&cinfo, TRUE);

  // QR codes are expressed as grayscale
  // Default for JPEG is to use 3 bytes per pixel - we want only 1
  cinfo.output_components = 1;
  cinfo.out_color_space = JCS_GRAYSCALE;
  jpeg_start_decompress(&cinfo);

  myImageInfo = new ImageInfo();
  myImageInfo->width = cinfo.image_width;
  myImageInfo->height = cinfo.image_height;
  myImageInfo->numComponents = cinfo.output_components;
  myImageInfo->data = new uint8_t[myImageInfo->width * myImageInfo->height * myImageInfo->numComponents];

  while (cinfo.output_scanline < cinfo.image_height) {
    uint8_t* p = myImageInfo->data + cinfo.output_scanline * cinfo.image_width * cinfo.output_components;
    jpeg_read_scanlines(&cinfo, &p, 1);
  }

  jpeg_finish_decompress(&cinfo);
  jpeg_destroy_decompress(&cinfo);
  fclose(pFile);

  return myImageInfo;
}

void JpegConverter::Save(const std::string& filename) {
  if (myImageInfo == NULL) {
    throw std::invalid_argument("No image loaded");
  }

  FILE* outfile = fopen(filename.c_str(), "wb");

  if (!outfile) {
    throw std::invalid_argument("file not found");
  }

  struct jpeg_compress_struct cinfo;
  struct jpeg_error_mgr jerr;

  cinfo.err = jpeg_std_error(&jerr);
  jpeg_create_compress(&cinfo);
  jpeg_stdio_dest(&cinfo, outfile);

  cinfo.image_width = myImageInfo->width;
  cinfo.image_height = myImageInfo->height;
  cinfo.input_components = 1;
  cinfo.in_color_space = JCS_GRAYSCALE;

  jpeg_set_defaults(&cinfo);
  /*set the quality [0..100]  */
  jpeg_set_quality(&cinfo, 100, true);
  jpeg_start_compress(&cinfo, true);

  JSAMPROW row_pointer; /* pointer to a single row */

  unsigned int width = myImageInfo->width;
  while (cinfo.next_scanline < cinfo.image_height) {
    row_pointer = (JSAMPROW)(myImageInfo->data + cinfo.next_scanline * width * cinfo.input_components);
    jpeg_write_scanlines(&cinfo, &row_pointer, 1);
  }

  jpeg_finish_compress(&cinfo);
  fclose(outfile);
}

void JpegConverter::Cleanup() {
  if (myImageInfo) {
    delete[] myImageInfo->data;
    delete myImageInfo;
    myImageInfo = NULL;
  }
}

void JpegConverter::ErrorExit(j_common_ptr cinfo) {
  // cinfo->err is actually a pointer to my_error_mgr.defaultErrorManager, since pub
  // is the first element of my_error_mgr we can do a sneaky cast
  ErrorManager* pErrorManager = (ErrorManager*)cinfo->err;
  (*cinfo->err->output_message)(cinfo);  // print error message (actually disabled below)
  longjmp(pErrorManager->jumpBuffer, 1);
}

void JpegConverter::OutputMessage(j_common_ptr cinfo) {
  // disable error messages
  /*char buffer[JMSG_LENGTH_MAX];
  (*cinfo->err->format_message) (cinfo, buffer);
  fprintf(stderr, "%s\n", buffer);*/
}

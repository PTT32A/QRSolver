#include <log.h>
#include "BufferConverter.h"

size_t BufferConverter::GetScaledSize(size_t width, size_t height, unsigned int factor) const {
  return (width *= factor) * (height *= factor);
}

void BufferConverter::ScaleBuffer(ChangeData* image, unsigned int factor) const {
  size_t newSize = GetScaledSize(image->width, image->height, factor);
  size_t newWidth = image->width * factor;
  ::memset(image->outbuffer, 0, newSize);

  for (size_t iH(0); iH < image->height; ++iH) {
    size_t targetLineStart = ((iH * factor) * newWidth);

    // Copy individual values <factor> times (expand horizontally)
    for (size_t iW(0); iW < image->width; ++iW) {
      size_t sourcePos = iH * image->width + iW;
      uint8_t val = image->buffer[sourcePos];
      for (unsigned int i(0); i < factor; ++i) {
        image->outbuffer[targetLineStart + (iW * factor) + i] = val;
      }
    }

    // Copy line <factor> times (expand vertically)
    for (unsigned int i(1); i < factor; ++i) {
      memcpy(&image->outbuffer[targetLineStart + i * newWidth], &image->outbuffer[targetLineStart], newWidth);
    }
  }
}

size_t BufferConverter::GetPaddedSize(size_t width, size_t height, unsigned int padding) const {
  size_t paddedWidth = width + 2 * padding;

  // the horizontal lines on top/bottom
  size_t paddingLines = paddedWidth * padding * 2;

  // starting / trailing padding on every line
  size_t paddingEdges = height * (2 * padding);

  return (width * height) + paddingLines + paddingEdges;
}

void BufferConverter::PadBuffer(ChangeData* image, unsigned int padding, uint8_t paddingColor /*= 255*/) const {
  if (padding == 0) {
    memcpy(image->outbuffer, image->buffer, image->width * image->height);
    return;
  }

  size_t paddedSize = GetPaddedSize(image->width, image->height, padding);
  memset(image->outbuffer, paddingColor, paddedSize);

  size_t paddedWidth = image->width + 2 * padding;

  size_t currentIndex = paddedWidth * padding;  // skip past top padding lines
  for (size_t i(0); i < image->height; ++i) {
    currentIndex += padding;  // pad left
    memcpy(&image->outbuffer[currentIndex], &image->buffer[i * image->width], image->width);
    currentIndex += (image->width + padding);  // copied content + right padding
  }
}

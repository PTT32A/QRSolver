#include <string.h>
#include <iomanip>
#include <sstream>

#include <cppcodec/base64_default_rfc4648.hpp>
#include "DataString.h"

namespace {
const int WIDTH = 3;
}

std::string DataString::ToString(uint8_t *const data, size_t size) {
  if (size < 1) {
    return "";
  }

  std::stringstream ss;
  ss << std::setfill('0');
  for (size_t i(0); i < size; ++i) {
    ss << std::setw(WIDTH) << std::to_string(data[i]);
  }
  std::string encoded = base64::encode(ss.str());
  return encoded;
}

std::vector<uint8_t> DataString::ToData(const std::string &dataString) {
  std::vector<uint8_t> decoded = base64::decode(dataString);

  size_t size = decoded.size();
  if (size < 1) {
    return std::vector<uint8_t>();
  }
  if (size % WIDTH > 0) {
    throw std::invalid_argument("string could not be formatted to byte array");
  }

  std::vector<uint8_t> output;
  for (size_t i(0); i < size; i += WIDTH) {
    std::string substr((char *)&decoded[i], WIDTH);
    output.push_back(std::stoi(substr));
  }
  return output;
}

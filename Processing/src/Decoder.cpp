#include <errno.h>
#include <log.h>
#include <stdio.h>
#include <stdlib.h>
#include <sstream>
#include <stdexcept>
#include "Decoder.h"
#include "JpegConverter.h"

Decoder::Decoder() { qr = quirc_new(); }

Decoder::~Decoder() {
  if (qr != NULL) {
    quirc_destroy(qr);
  }
}

std::string Decoder::ReadJpeg(const std::string& filename, bool copyTemp /* = false*/) {
  JpegConverter converter;
  const JpegConverter::ImageInfo* image = converter.Load(filename.c_str());

  if (image == NULL) {
    throw std::invalid_argument("File not found: " + filename);
  }

  if (copyTemp) {
    converter.Save("/tmp/" + filename);
  }

  int width = image->width;
  int height = image->height;

  return ReadBuffer(image->data, width, height);
}

std::string Decoder::ReadBuffer(uint8_t* buffer, int width, int height) {
  if (qr == NULL) {
    throw std::runtime_error("Failed to allocate qr memory");
  }

  if (quirc_resize(qr, width, height) < 0) {
    throw std::runtime_error("Failed to resize");
  }
  uint8_t* qrImage = quirc_begin(qr, NULL, NULL);
  memcpy(qrImage, buffer, width * height * sizeof(uint8_t));
  quirc_end(qr);
  return Extract();
}

std::string Decoder::Extract() {
  if (!qr) {
    throw std::runtime_error("Failed to allocate qr memory");
  }
  std::string retval;

  int num_codes = quirc_count(qr);
  if (num_codes > 0) {
    struct quirc_code code;
    struct quirc_data data;
    quirc_decode_error_t err;

    quirc_extract(qr, 0, &code);

    /* Decoding stage */
    err = quirc_decode(&code, &data);
    if (err) {
      std::string errText = "Decode failed: " + std::string(quirc_strerror(err));
      throw std::runtime_error(errText);
    }
    retval = std::string((char*)data.payload);
    LOG(INFO) << "Payload: " << retval;
  } else {
    LOG(WARNING) << "No codes found";
  }

  return retval;
}

std::string Decoder::GetString(uint8_t* buffer, int width, int height) {
  std::stringstream ss;
  ss << "\n{";
  for (int i = 0; i < height; ++i) {
    for (int j = 0; j < width; ++j) {
      ss << std::to_string(buffer[i * j + j]) << ", ";
    }
    ss << "\n";
  }

  ss.seekp(-2, ss.cur);
  ss << "}";
  return ss.str();
}

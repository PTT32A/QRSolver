#pragma once

extern "C" {
#include <quirc.h>
}

#include <string>

class Decoder {
 public:
  Decoder();
  virtual ~Decoder();

  /*
  Pre: -
  Post: Jpeg picture in location <filename> is scanned for QR codes.
    The contents of the first code found are returned as string.
    If copyTemp was enabled, a copy of the file was made to /tmp/<filename>
  Returns: a string containing the content of scanned QR code. Empty string if no valid QR code was found.

  Throws std::invalid_argument if <filename> is invalid.
  */
  std::string ReadJpeg(const std::string& filename, bool copyTemp = false);

  /*
  Pre: <buffer> is a valid pointer to an array of uint8_t[width * height]
    Buffer contains a monochrome image, with 1 byte per pixel.
    Image width and height correspond to <width> and <height>
  Post: buffer is unmodified. Contents of the first found QR code are returned as string.
  Returns: a string containing the content of scanned QR code. Empty string if no valid QR code was found.
  */
  std::string ReadBuffer(uint8_t* buffer, int width, int height);

 private:
  std::string Extract();
  std::string GetString(uint8_t* buffer, int width, int height);

 private:
  struct quirc* qr;
};

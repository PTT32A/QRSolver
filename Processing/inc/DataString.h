#pragma once

#include <stdio.h>
#include <stdexcept>
#include <string>
#include <vector>

class DataString {
 public:
  static std::string ToString(uint8_t* const data, size_t size);
  static std::vector<uint8_t> ToData(const std::string& dataString);
};

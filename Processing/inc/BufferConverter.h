#pragma once

#include <stdint.h>
#include <stdio.h>

class BufferConverter {
 public:
  // Base data required for a conversion operation
  struct ChangeData {
    // The length of a given horizontal line in the image
    size_t width;

    // The number of horizontal lines in the image
    size_t height;

    // The buffer containing the image data. This should be width * height
    uint8_t* buffer;

    // The buffer where output data of a conversion will be written
    // Its required size can be found by calling Get<Converted>Size() functions
    uint8_t* outbuffer;
  };

 public:
  /*
  Pre: height, width, factor are positive and > 0
  Post: resulting outbuffer size after scale operation with factor <factor> is calculated
  Returns: required size of outbuffer when calling ScaleBuffer(image, factor)
  */
  size_t GetScaledSize(size_t width, size_t height, unsigned int factor) const;

  /*
  Pre: image is initialized properly, factor and size of image->outbuffer correspond to values used in GetScaledSize()
  Post: image->outbuffer contains the image in image->buffer, enlarged with factor <factor>

  The buffer is enlarged using a nearest-neighbor implementation.
  Every pixel in <buffer> will be written <factor> times in both horizontal and vertical directions into <outbuffer>.
  For example, when calling ScaleBuffer(image, 2), the image
  0, 1,
  2, 3
  would become:
  0, 0, 1, 1,
  0, 0, 1, 1,
  2, 2, 3, 3,
  2, 2, 3, 3

  Calling ScaleBuffer(image, 1) is equivalent to doing a memcpy() of buffer into outbuffer.
  */
  void ScaleBuffer(ChangeData* image, unsigned int factor) const;

  /*
  Pre: height, width, factor are positive and > 0
  Post: resulting outbuffer size after padding operation with <padding> is calculated
  Returns: required size of outbuffer when calling PadBuffer(image, padding)
  */
  size_t GetPaddedSize(size_t width, size_t height, unsigned int padding) const;

  /*
  Pre: image is initialized properly, padding and size of image->outbuffer correspond to values used in GetPaddedSize()
  Post: image->outbuffer contains the image in image->buffer, padded with a border of <padding> pixels of value
  <paddingColor>

  The buffer is padded with pixels on all borders. <padding> declares the width of the border, and <paddingColor> the
  value for those pixels.
  For example, when calling PadBuffer(image, 1, 0), the image
  1, 2,
  3, 4
  would become:
  0, 0, 0, 0,
  0, 1, 2, 0,
  0, 3, 4, 0,
  0, 0, 0, 0
  */
  void PadBuffer(ChangeData* image, unsigned int padding, uint8_t paddingColor = 255) const;
};

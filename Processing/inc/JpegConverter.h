// Code based on JpegConverter.h from "How to use libJpeg in a C++ project"  (https://numberduck.com/Blog/?nPostId=2)
// Downloaded 2016-12-17

#pragma once

// Needs to be included before jpeglib.h
#include <setjmp.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#include <jpeglib.h>
#include <string>

class JpegConverter {
 public:
  // Container for Jpeg image data
  struct ImageInfo {
    // Horizontal line length of the image
    uint32_t width;
    // Number of horizontal lines present in the image
    uint32_t height;
    // Number of bytes per pixel
    uint8_t numComponents;
    // Backing byte array. Size is width * height * numComponents
    uint8_t* data;
  };

  JpegConverter();
  ~JpegConverter();

  /*
  Pre: -
  Post: Previously held image was discarded, and replaced with the image at <filename>
  Returns: a pointer to its new image, or NULL on error.
  */
  const ImageInfo* Load(const std::string& filename);

  /*
  Pre: An image was loaded.
  Post: Image is converted to the Jpeg format, and saved to disk as <filename>

  Throws:
    - std::invalid_argument if unable to open <filename>
    - std::invalid_argument if no image was loaded previously
  */
  void Save(const std::string& filename);

  /*
  Pre: -
  Post: Previously held image was discarded, and replaced with a copy of <input>
  Returns: a pointer to the memory address of the new image.

  This function can be called as SetImage(NULL)
  It will cause the current image to be discarded, and not replaced.
  */
  const ImageInfo* SetImage(ImageInfo* input);

  /*
  Pre: -
  Post: -
  Returns: a pointer to currently held image, or NULL if no image was set or loaded.
  */
  const ImageInfo* GetImage() const { return myImageInfo; }

 private:
  ImageInfo* myImageInfo;
  void Cleanup();

  struct ErrorManager {
    jpeg_error_mgr defaultErrorManager;
    jmp_buf jumpBuffer;
  };

  static void ErrorExit(j_common_ptr cinfo);
  static void OutputMessage(j_common_ptr cinfo);

 private:  // block copy constructor
  JpegConverter(JpegConverter&) {}
};

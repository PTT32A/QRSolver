#! /bin/sh

BASE_OUTPUT='coverage/base_output.info'
TEST_OUTPUT='coverage/test_output.info'
TOTAL_OUTPUT='coverage/total_output.info'

# script will fail on error
set -e

# Rebuild project
make cleaner
make

mkdir -p coverage

# Generate base report
lcov -q --base-directory . -d . --zerocounters
lcov -q --base-directory . -d . -c -i -o $BASE_OUTPUT

# Run tests and generate report
make test
echo "Generating code coverage report..."
lcov -q --base-directory . -d . -c -o $TEST_OUTPUT

# Merge base and test report
lcov -q -a $BASE_OUTPUT -a $TEST_OUTPUT -o $TOTAL_OUTPUT

# Remove coverage of irrelevant code
lcov -q --remove $TOTAL_OUTPUT "/usr*" -o $TOTAL_OUTPUT
lcov -q --remove $TOTAL_OUTPUT "Demo*" -o $TOTAL_OUTPUT 
lcov -q --remove $TOTAL_OUTPUT "Utils*" -o $TOTAL_OUTPUT

# Generate html report
rm -rf coverage/output
genhtml -q -o coverage/output -t "QRSolver test coverage" --num-spaces 4 $TOTAL_OUTPUT

#! /bin/sh

sudo dpkg --add-architecture armel
sudo apt-get update
sudo apt-get install -y aptitude
sudo aptitude install -y libjpeg-dev:armel libsdl1.2-dev:armel libsdl-gfx1.2-dev:armel

sudo make setup-arm
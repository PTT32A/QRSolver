#include <SSTR.h>
#include <SocketChannel.h>
#include <log.h>
#include <semaphore.h>
#include <string.h>
#include "ScanStubListener.h"

INITIALIZE_EASYLOGGINGPP

namespace {
sem_t exitSem;
const int PORT = 8500;

std::shared_ptr<SocketChannel> channel;
std::shared_ptr<ScanStubListener> listener;
}

void SigCatcher(int signum, siginfo_t* info, void* ptr) {
  LOG(INFO) << "caught " << strsignal(signum);
  listener.reset();
  channel.reset();

  sem_post(&exitSem);
}

int main(int argc, char** argv) {
  el::Configurations conf("logger.conf");
  el::Loggers::reconfigureLogger("default", conf);

  std::string QRFile;
  if (argc == 1) {
    LOG(ERROR) << "QR code image not specified";
  } else {
    QRFile = argv[1];
  }

  sem_init(&exitSem, 0, 0);

  // ensures SIGTERM and SIGINT are caught and handled properly
  struct sigaction action;
  memset(&action, 0, sizeof(struct sigaction));
  action.sa_sigaction = SigCatcher;
  action.sa_flags = SA_SIGINFO;
  sigaction(SIGTERM, &action, NULL);
  sigaction(SIGINT, &action, NULL);

  // Listens to channel
  channel = std::make_shared<SocketChannel>(PORT);
  listener = std::make_shared<ScanStubListener>();
  listener->SetQRFile(QRFile);
  channel->AddListener(listener);
  channel->Start();

  sem_wait(&exitSem);
  LOG(INFO) << "Shutting down scan client stub...";

  return 0;
}

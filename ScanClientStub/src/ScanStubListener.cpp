#include <Connection.h>
#include <DataString.h>
#include <JpegConverter.h>
#include <Message.h>
#include <SSTR.h>
#include <log.h>
#include <stringsplit.h>
#include "ScanStubListener.h"

void ScanStubListener::OnMessage(const Message& message, MessageReply* reply) {
  serverInfo = message.GetSenderAddress();
  Connection con;
  con.Connect(serverInfo.address, SERVER_PORT);  // setup socket to send data over

  std::string content = message.GetContent();
  auto split = stringsplit::split(content, DELIM[0]);
  LOG(INFO) << "Server says: " << content;

  if (content.find("STARTSCAN") == 0) {
    SendImage(&con);
  } else if (content.find("STOPSEND") == 0) {
    Shutdown(true);
  } else if (content.find("hello") == 0) {
    name = split[1];
    con.Send(SSTR("SCANNER READY" << DELIM << name));
  }
}

void ScanStubListener::SendImage(Connection* con) {
  JpegConverter converter;
  const JpegConverter::ImageInfo* image = converter.Load(qrFile);
  LOG(INFO) << "Sending file: " << qrFile;

  if (image == NULL) {
    LOG(ERROR) << "Invalid image: " << qrFile;
    return;
  }

  con->Send(SSTR("STARTSEND" << DELIM << name));
  for (size_t i(0); i < image->height; ++i) {
    std::string dataString = DataString::ToString(&image->data[i * image->width], image->width);
    con->Send(dataString);
  }
  con->Send(SSTR("STOPSEND" << DELIM << name << DELIM << image->width << DELIM << image->height));
}

void ScanStubListener::Shutdown(bool exit) {
  if (serverInfo.valid) {
    try {
      Connection con;
      con.Connect(serverInfo.address, SERVER_PORT);
      con.Send(SSTR("SCANNER SHUTDOWN" << DELIM << name));
    } catch (SocketException& ex) {
      LOG(ERROR) << "Socket exception shutting down: " << ex.description();
    } catch (std::exception& ex) {
      LOG(ERROR) << "Exception shutting down: " << ex.what();
    }
  }
  if (exit && exitSem != NULL) {
    sem_post(exitSem);
  }
}

#pragma once

#include <AddressInfo.h>
#include <Connection.h>
#include <IMessageListener.h>
#include <JpegConverter.h>
#include <semaphore.h>
#include <string>

class ScanStubListener : public IMessageListener {
 public:
  ScanStubListener() {}
  virtual ~ScanStubListener() { Shutdown(true); }

  void Shutdown(bool exit);

 public:  // IMessageListener functions
  virtual void OnMessage(const Message& message, MessageReply* reply);
  virtual void OnError(const std::string& error) {}
  virtual void OnConnect() {}
  virtual void OnDisconnect() {}

 public:
  void SetQRFile(const std::string& file) { qrFile = file; }
  void SetExitSem(sem_t* sem) { exitSem = sem; }

 private:
  void SendImage(Connection* con);

 private:
  std::string qrFile;
  AddressInfo serverInfo;
  const int SERVER_PORT = 8501;
  std::string name;
  sem_t* exitSem;

  const std::string DELIM = ";";
};

#! python

pretty_pictures = True

def check_val(val, idx):
    shifted = val >> idx
    return (shifted & 1) == 1

def print_picture(val):
    output = ""
    output += "_____" if check_val(val, 0) else "     "
    output += " \n"
    output += "|" if check_val(val, 3) else " "
    output += "\\" if check_val(val, 2) else " "
    output += "|" if check_val(val, 6) else " "
    output += "/" if check_val(val, 1) else " "
    output += "|" if check_val(val, 11) else " "
    output += "\n"
    output += "--" if check_val(val, 4) else "  "
    output += " "
    output += "--" if check_val(val, 5) else "  "
    output += "\n"
    output += "|" if check_val(val, 8) else " "
    output += "/" if check_val(val, 12) else " "
    output += "|" if check_val(val, 7) else " "
    output += "\\" if check_val(val, 13) else " "
    output += "|" if check_val(val, 10) else " "
    output += "\n"
    output += "-----" if check_val(val, 9) else "     "
    print output

def generate(char, val):
    if pretty_pictures:
        print_picture(val)
    else:
        output = "{{ '{0}' , {1} }},".format(char, val)
        print output

generate("a", 0b00110100111001)
generate("b", 0b00111011100001)
generate("c", 0b00001100001001)
generate("d", 0b00111011000001)
generate("e", 0b00001100011001)
generate("f", 0b00000100011001)
generate("g", 0b00011100101001)
generate("h", 0b00110100111000)
generate("i", 0b00001011000001)
generate("j", 0b00111100000000)
generate("k", 0b10000100011010)
generate("l", 0b00001100001000)
generate("m", 0b00110100001110)
generate("n", 0b10110100001100)
generate("o", 0b00111100001001)
generate("p", 0b00100100111001)
generate("q", 0b10111100001001)
generate("r", 0b10100100111001)
generate("s", 0b00011000111001)
generate("t", 0b00000011000001)
generate("u", 0b00111100001000)
generate("v", 0b01000100001010)
generate("w", 0b11110100001000)
generate("x", 0b11000000000110)
generate("y", 0b00000010000110)
generate("z", 0b01001000000011)
generate("1", 0b00000011000000)
generate("2", 0b00101100110001)
generate("3", 0b00111000110001)
generate("4", 0b00110000111000)
generate("5", 0b00011000111001)
generate("6", 0b00011100111001)
generate("7", 0b00110000000001)
generate("8", 0b00111100111001)
generate("9", 0b00111000111001)
generate("0", 0b01111100111001)
generate(" ", 0b00000000000000)
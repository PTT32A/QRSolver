#pragma once

#include <stdint.h>
#include <map>
#include <string>

class SegmentLib {
 public:
  static const std::map<char, uint16_t> font;
};
#pragma once

#include <string>
#include <map>

class FileConfigReader {
 public:
  std::map<std::string, std::string> Read(const std::string& address, const char delim = ':');
};

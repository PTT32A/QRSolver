#pragma once
#include <Motioncontroller.h>
#include "PlotterUI.h"
#include "Shapes.h"
#include <string>
#include <vector>
class PlotterUI;
class Plotter {
 public:
  Plotter();
  ~Plotter();
  void StartString(std::string content);
  void StartShape(shapes_enum::shapes_t shape);
  void Pause();
  void Cancel();
  void Resume();
  void AddClient(PlotterUI* client);

 private:
  void DrawString(int xPosition, int yPosition, std::string content);
  void DrawCharacter(int xPosition, int yPosition, uint16_t letter);
  void DrawLine(int xPosition, int yPosition, int index);
  void NotifyClients();

  Motioncontroller controller;
  std::vector<PlotterUI*> clients;
};

#pragma once
#include "PlotterUI.h"
#include <memory>

class PlotBrickUI : public PlotterUI {
 public:
  void OnPlot();
  void SetPlotter(std::shared_ptr<Plotter> plotterTarget);

 private:
  std::shared_ptr<Plotter> plotter;
};

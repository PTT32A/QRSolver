#pragma once

#include <IMessageListener.h>
#include <semaphore.h>
#include <string>
#include "Message.h"

class PlotterMessageListener : public IMessageListener {
 public:
  PlotterMessageListener();
  virtual ~PlotterMessageListener();

  void OnMessage(const Message& message, MessageReply* reply);
  void OnError(const std::string& error);
  void OnConnect();
  void OnDisconnect();

  void Shutdown(bool exit);
  void SetNotifier(sem_t* sem) { exitSem = sem; }

 private:
  AddressInfo serverInfo;
  const int SERVER_PORT = 8501;
  std::string name;
  sem_t* exitSem;

  std::shared_ptr<Plotter> PlotterHardware;
  std::shared_ptr<PlotBrickUI> PlotterClient;

  const std::string DELIM = ";";
};

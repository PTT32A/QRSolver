#pragma once
#include "Plotter.h"
class Plotter;
class PlotterUI {
 public:
  virtual void OnPlot() = 0;
  virtual void SetPlotter(std::shared_ptr<Plotter> plotter) = 0;
};

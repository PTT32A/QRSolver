#include <Connection.h>
#include <PlotBrickUI.h>
#include <Plotter.h>
#include <SSTR.h>
#include <log.h>
#include <stringsplit.h>
#include "PlotterMessageListener.h"

PlotterMessageListener::PlotterMessageListener() : exitSem(NULL) {
  PlotterHardware = std::make_shared<Plotter>();
  PlotterClient = std::make_shared<PlotBrickUI>();
}

PlotterMessageListener::~PlotterMessageListener() { Shutdown(false); }

void PlotterMessageListener::OnMessage(const Message& message, MessageReply* reply) {
  serverInfo = message.GetSenderAddress();
  Connection con;
  con.Connect(serverInfo.address, SERVER_PORT);  // setup socket to send data over

  std::string content = message.GetContent();
  auto split = stringsplit::split(content, DELIM[0]);
  LOG(INFO) << "Server says: " << content;

  if (content.find("STARTPLOT") == 0) {
    PlotterHardware->StartString(split.at(1));
    con.Send(SSTR("PLOTTER DONE" << DELIM << name));
  } else if (content.find("STOPSEND") == 0) {
    Shutdown(true);
  } else if (content.find("hello") == 0) {
    name = split[1];
    con.Send(SSTR("PLOTTER READY" << DELIM << name));
  }
}

void PlotterMessageListener::OnError(const std::string& error) {}

void PlotterMessageListener::OnConnect() {}

void PlotterMessageListener::OnDisconnect() {}

void PlotterMessageListener::Shutdown(bool exit) {
  if (serverInfo.valid) {
    try {
      Connection con;
      con.Connect(serverInfo.address, SERVER_PORT);
      con.Send(SSTR("PLOTTER SHUTDOWN" << DELIM << name));
    } catch (SocketException& ex) {
      LOG(ERROR) << "Socket exception shutting down: " << ex.description();
    } catch (std::exception& ex) {
      LOG(ERROR) << "Exception shutting down: " << ex.what();
    }
  }
  if (exit && exitSem != NULL) {
    sem_post(exitSem);
  }
}

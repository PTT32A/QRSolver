#include <iostream>
#include "PlotBrickUI.h"
#include "Plotter.h"

void PlotBrickUI::OnPlot() { std::cout << "callback: plotting compleet"; }
void PlotBrickUI::SetPlotter(std::shared_ptr<Plotter> plotterTarget) {
  plotter = plotterTarget;
  plotter->AddClient(this);
}

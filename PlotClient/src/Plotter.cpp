#include "Plotter.h"
#include <log.h>
#include <SegmentLib.h>
#include <iostream>

const int penDistance = 148;
const int liftOff = 100;
const int lineDistance = 160;
const int lineSegments = 13;

Plotter::Plotter() {}

Plotter::~Plotter() {}
void Plotter::StartString(std::string content) {
  transform(content.begin(), content.end(), content.begin(), (int (*)(int))tolower);
  controller.Home();  // go to 0,0 - is blocking
  int xPosition = 30;
  int yPosition = 2200;
  controller.AbsolutePtP(xPosition, yPosition, 0);
  DrawString(xPosition, yPosition, content);
  controller.AbsolutePtP(0, 0, 0);

  NotifyClients();
}

void Plotter::DrawString(int xPosition, int yPosition, std::string content) {
  for (size_t i = 0; i < content.length(); i++) {
    auto result = SegmentLib::font.find(content.at(i));
    if (result != SegmentLib::font.end()) {
      uint16_t letter = result->second;
      LOG(INFO) << "Going to draw letter: " << content.at(i) << " with code: " << letter;
      xPosition += (lineDistance * 1.5);
      DrawCharacter(xPosition, yPosition, letter);
    } else {
      std::cout << "char " << content.at(i) << "not found" << std::endl;
    }
  }
}

void Plotter::DrawCharacter(int xPosition, int yPosition, uint16_t letter) {
  for (size_t i = 0; i < lineSegments + 1; i++) {
    if (letter & (1 << (i))) {  // check if segment has to be written
      DrawLine(xPosition, yPosition, i);
    }
  }
}

void Plotter::DrawLine(int xPosition, int yPosition, int index) {
  LOG(INFO) << "Drawing line at X,Y: " << xPosition << "," << yPosition << " with index: " << index;
  switch (index) {
    case 0:
      controller.AbsolutePtP(xPosition, yPosition, liftOff);
      controller.AbsolutePtP(xPosition, yPosition, penDistance);
      controller.AbsolutePtP(xPosition + lineDistance, yPosition, penDistance);
      controller.AbsolutePtP(xPosition + lineDistance, yPosition, liftOff);
      break;
    case 1:
      controller.AbsolutePtP(xPosition + lineDistance, yPosition, liftOff);
      controller.AbsolutePtP(xPosition + lineDistance, yPosition, penDistance);
      controller.AbsolutePtP(xPosition + (lineDistance / 2), yPosition - (lineDistance / 2), penDistance);
      controller.AbsolutePtP(xPosition + (lineDistance / 2), yPosition - (lineDistance / 2), liftOff);
      break;
    case 2:
      controller.AbsolutePtP(xPosition + (lineDistance / 2), yPosition - (lineDistance / 2), liftOff);
      controller.AbsolutePtP(xPosition + (lineDistance / 2), yPosition - (lineDistance / 2), penDistance);
      controller.AbsolutePtP(xPosition, yPosition, penDistance);
      controller.AbsolutePtP(xPosition, yPosition, liftOff);
      break;
    case 3:
      controller.AbsolutePtP(xPosition, yPosition, liftOff);
      controller.AbsolutePtP(xPosition, yPosition, penDistance);
      controller.AbsolutePtP(xPosition, yPosition - (lineDistance / 2), penDistance);
      controller.AbsolutePtP(xPosition, yPosition - (lineDistance / 2), liftOff);
      break;
    case 4:
      controller.AbsolutePtP(xPosition, yPosition - (lineDistance / 2), liftOff);
      controller.AbsolutePtP(xPosition, yPosition - (lineDistance / 2), penDistance);
      controller.AbsolutePtP(xPosition + (lineDistance / 2), yPosition - (lineDistance / 2), penDistance);
      controller.AbsolutePtP(xPosition + (lineDistance / 2), yPosition - (lineDistance / 2), liftOff);
      break;
    case 5:
      controller.AbsolutePtP(xPosition + (lineDistance / 2), yPosition - (lineDistance / 2), liftOff);
      controller.AbsolutePtP(xPosition + (lineDistance / 2), yPosition - (lineDistance / 2), penDistance);
      controller.AbsolutePtP(xPosition + lineDistance, yPosition - (lineDistance / 2), penDistance);
      controller.AbsolutePtP(xPosition + lineDistance, yPosition - (lineDistance / 2), liftOff);
      break;
    case 6:
      controller.AbsolutePtP(xPosition + (lineDistance / 2), yPosition, liftOff);
      controller.AbsolutePtP(xPosition + (lineDistance / 2), yPosition, penDistance);
      controller.AbsolutePtP(xPosition + (lineDistance / 2), yPosition - (lineDistance / 2), penDistance);
      controller.AbsolutePtP(xPosition + (lineDistance / 2), yPosition - (lineDistance / 2), liftOff);
      break;
    case 7:
      controller.AbsolutePtP(xPosition + (lineDistance / 2), yPosition - (lineDistance / 2), liftOff);
      controller.AbsolutePtP(xPosition + (lineDistance / 2), yPosition - (lineDistance / 2), penDistance);
      controller.AbsolutePtP(xPosition + (lineDistance / 2), yPosition - lineDistance, penDistance);
      controller.AbsolutePtP(xPosition + (lineDistance / 2), yPosition - lineDistance, liftOff);
      break;
    case 8:
      controller.AbsolutePtP(xPosition, yPosition - (lineDistance / 2), liftOff);
      controller.AbsolutePtP(xPosition, yPosition - (lineDistance / 2), penDistance);
      controller.AbsolutePtP(xPosition, yPosition - lineDistance, penDistance);
      controller.AbsolutePtP(xPosition, yPosition - lineDistance, liftOff);
      break;
    case 9:
      controller.AbsolutePtP(xPosition, yPosition - lineDistance, liftOff);
      controller.AbsolutePtP(xPosition, yPosition - lineDistance, penDistance);
      controller.AbsolutePtP(xPosition + lineDistance, yPosition - lineDistance, penDistance);
      controller.AbsolutePtP(xPosition + lineDistance, yPosition - lineDistance, liftOff);
      break;
    case 10:
      controller.AbsolutePtP(xPosition + lineDistance, yPosition - lineDistance, liftOff);
      controller.AbsolutePtP(xPosition + lineDistance, yPosition - lineDistance, penDistance);
      controller.AbsolutePtP(xPosition + lineDistance, yPosition - (lineDistance / 2), penDistance);
      controller.AbsolutePtP(xPosition + lineDistance, yPosition - (lineDistance / 2), liftOff);
      break;
    case 11:
      controller.AbsolutePtP(xPosition + lineDistance, yPosition - (lineDistance / 2), liftOff);
      controller.AbsolutePtP(xPosition + lineDistance, yPosition - (lineDistance / 2), penDistance);
      controller.AbsolutePtP(xPosition + lineDistance, yPosition, penDistance);
      controller.AbsolutePtP(xPosition + lineDistance, yPosition, liftOff);
      break;
    case 12:
      controller.AbsolutePtP(xPosition + (lineDistance / 2), yPosition - (lineDistance / 2), liftOff);
      controller.AbsolutePtP(xPosition + (lineDistance / 2), yPosition - (lineDistance / 2), penDistance);
      controller.AbsolutePtP(xPosition, yPosition - lineDistance, penDistance);
      controller.AbsolutePtP(xPosition, yPosition - lineDistance, liftOff);
      break;
    case 13:
      controller.AbsolutePtP(xPosition + lineDistance, yPosition - lineDistance, liftOff);
      controller.AbsolutePtP(xPosition + lineDistance, yPosition - lineDistance, penDistance);
      controller.AbsolutePtP(xPosition + (lineDistance / 2), yPosition - (lineDistance / 2), penDistance);
      controller.AbsolutePtP(xPosition + (lineDistance / 2), yPosition - (lineDistance / 2), liftOff);
      break;
  }
}

void Plotter::StartShape(shapes_enum::shapes_t shape) {
  controller.Home();  // go to 0,0 - is blocking
  switch (shape) {
    case shapes_enum::LINE:
      controller.AbsolutePtP(1000, 1000, 0);
      controller.AbsolutePtP(1000, 1000, penDistance);
      controller.AbsolutePtP(2000, 1000, penDistance);
      controller.AbsolutePtP(2000, 1000, 0);
      controller.AbsolutePtP(0, 0, 0);
      break;
    case shapes_enum::SQUARE:
      controller.AbsolutePtP(1000, 1000, 0);
      controller.AbsolutePtP(1000, 1000, penDistance);
      controller.AbsolutePtP(2000, 1000, penDistance);
      controller.AbsolutePtP(2000, 2000, penDistance);
      controller.AbsolutePtP(1000, 2000, penDistance);
      controller.AbsolutePtP(1000, 1000, penDistance);
      controller.AbsolutePtP(1000, 1000, 0);
      controller.AbsolutePtP(0, 0, 0);
      break;
    case shapes_enum::TRIANGLE:
      controller.AbsolutePtP(1000, 1000, 0);
      controller.AbsolutePtP(1000, 1000, penDistance);
      controller.AbsolutePtP(2000, 1000, penDistance);
      controller.AbsolutePtP(1500, 1500, penDistance);
      controller.AbsolutePtP(1000, 1000, penDistance);
      controller.AbsolutePtP(1000, 1000, 0);
      controller.AbsolutePtP(0, 0, 0);
      break;
  }

  NotifyClients();
}

void Plotter::Pause() {}
void Plotter::Cancel() { controller.Home(); }
void Plotter::Resume() {}
void Plotter::AddClient(PlotterUI *client) { clients.push_back(client); }
void Plotter::NotifyClients() {
  for (size_t i = 0; i < clients.size(); i++) {
    clients.at(i)->OnPlot();
  }
}

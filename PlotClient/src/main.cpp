#include <PlotBrickUI.h>
#include <Plotter.h>
#include <SocketChannel.h>
#include <log.h>
#include <semaphore.h>
#include <stringsplit.h>
#include <memory>
#include "PlotterMessageListener.h"

INITIALIZE_EASYLOGGINGPP

namespace {
const int PORT = 8500;

std::shared_ptr<SocketChannel> channel;
std::shared_ptr<PlotterMessageListener> listener;

sem_t exitSem;
}

void SigCatcher(int signum, siginfo_t* info, void* ptr) {
  LOG(INFO) << "caught " << strsignal(signum);
  listener.reset();
  channel.reset();

  sem_post(&exitSem);
}

int main(int argc, char* argv[]) {
  if (argc == 2) {
    std::shared_ptr<Plotter> PlotterHardware = std::make_shared<Plotter>();
    std::shared_ptr<PlotBrickUI> PlotterClient = std::make_shared<PlotBrickUI>();

    PlotterClient->SetPlotter(PlotterHardware);
    PlotterHardware->StartString(argv[1]);
    return 0;
  }

  sem_init(&exitSem, 0, 0);

  // ensures SIGTERM and SIGINT are caught and handled properly
  struct sigaction action;
  memset(&action, 0, sizeof(struct sigaction));
  action.sa_sigaction = SigCatcher;
  action.sa_flags = SA_SIGINFO;
  sigaction(SIGTERM, &action, NULL);
  sigaction(SIGINT, &action, NULL);

  channel = std::make_shared<SocketChannel>(PORT);
  listener = std::make_shared<PlotterMessageListener>();
  listener->SetNotifier(&exitSem);
  channel->AddListener(listener);
  channel->Start();

  sem_wait(&exitSem);

  return 0;
}

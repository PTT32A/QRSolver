#!/usr/bin/env python

'''
    Simple socket server using threads
'''

import socket
import sys
import signal


class GracefulKiller:
    kill_now = False

    def __init__(self):
        signal.signal(signal.SIGINT, self.exit_gracefully)
        signal.signal(signal.SIGTERM, self.exit_gracefully)

    def exit_gracefully(self, signum, frame):
        self.kill_now = True

def main():
    killer = GracefulKiller()
    host = ''   # Symbolic name, meaning all available interfaces
    port = int(sys.argv[1])  # Arbitrary non-privileged port

    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    print 'Socket created'

    # Bind socket to local host and port
    try:
        sock.bind((host, port))
    except socket.error as msg:
        print 'Bind failed. Error Code : ' + str(msg[0]) + ' Message ' + msg[1]
        sys.exit()

    print 'Socket bind complete'

    # Start listening on socket
    sock.listen(10)
    print 'Socket now listening'

    connected = False
    sock.settimeout(1)
    conn = None
    # wait to accept a connection - blocking call
    while not killer.kill_now and not connected:
        conn, addr = sock.accept()
        print 'Connected with ' + addr[0] + ':' + str(addr[1])
        connected = True

    # now keep talking with the client
    while not killer.kill_now:
        try:
            data = conn.recv(1024)
            if data:
                print "received: " + data
                conn.send(data)
                if data == "exit":
                    break
        except socket.error as msg:
            print "recv error: " + msg[1]

    sock.close()


if __name__ == '__main__':
    main()

#pragma once

#include <IMessageListener.h>
#include <Message.h>
#include <SocketChannel.h>
#include <log.h>
#include <chrono>
#include <condition_variable>
#include <mutex>
#include <queue>
#include <string>
#include <thread>
#include <vector>

class MessageListenerImpl : public IMessageListener {
 public:
  inline void OnMessage(const Message& message, MessageReply* reply) {
    std::unique_lock<std::mutex> lk(mtx);
    // LOG(INFO) << "On listener message: " << message.GetContent();
    received.push_back(message);

    if (echoServer) {
      reply->reply = message.GetContent();
    } else if (!replies.empty()) {
      reply->reply = replies.front();
      // LOG(INFO) << "Replying \"" << replies.front() << "\" to \"" << message.GetContent() << "\"";
      replies.pop();
    }

    cv.notify_one();
  }

  inline void OnError(const std::string& error) {}
  inline void OnConnect() {}
  inline void OnDisconnect() {}

  inline Message ExpectMessage(int TimeoutMs) {
    std::unique_lock<std::mutex> lk(mtx);
    // LOG(INFO) << "Received count: " << received.size();
    if (received.empty()) {
      cv.wait_for(lk, std::chrono::milliseconds(TimeoutMs), [this] { return !received.empty(); });
    }

    if (received.empty()) {
      throw std::runtime_error("Expect Message timed out");
    }

    Message retVal = received[0];
    received.erase(received.begin());
    return retVal;
  }

  inline void QueueReply(const std::string& newReply) { replies.push(newReply); }
  inline void SetEchoServer(bool newEcho) { echoServer = newEcho; }

 private:
  std::vector<Message> received;
  std::mutex mtx;
  std::condition_variable cv;

  std::queue<std::string> replies;
  bool echoServer = false;
};

#include <log.h>
#include "gtest/gtest.h"

INITIALIZE_EASYLOGGINGPP

int main(int argc, char** argv) {
  el::Configurations conf("logger.conf");
  el::Loggers::reconfigureLogger("default", conf);
  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}

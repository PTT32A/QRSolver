#include <Decoder.h>
#include <JpegConverter.h>
#include <log.h>
#include <math.h>
#include <string>
#include <vector>
#include "gtest/gtest.h"

TEST(DecoderTest, Test_ReadJpeg) {
  std::string qrText = "Hello world!";

  std::vector<std::string> goodCodes{"qrcode", "qrcode_double_noborder"};

  // The QR code decoder has limitations.
  // These include:
  // really small files (1 px / block)
  // black borders in small files
  std::vector<std::string> badCodes{"qrcode_small_border", "qrcode_double_border", "qrcode_double_nopadding",
                                    "qrcode_small_noborder", "qrcode_small_M"};

  Decoder decoder;

  for (size_t i = 0; i < goodCodes.size(); ++i) {
    std::string current = "qr/" + goodCodes.at(i) + ".jpeg";
    LOG(INFO) << "Reading good code: " << current;
    ASSERT_EQ(qrText, decoder.ReadJpeg(current));
  }

  for (size_t i(0); i < badCodes.size(); ++i) {
    std::string current = "qr/" + badCodes.at(i) + ".jpeg";
    LOG(INFO) << "Reading bad code: " << current;
    ASSERT_EQ("", decoder.ReadJpeg(current));
  }

  ASSERT_THROW(decoder.ReadJpeg("nonsense"), std::invalid_argument);
  ASSERT_THROW(decoder.ReadJpeg(""), std::invalid_argument);
}

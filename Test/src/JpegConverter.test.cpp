#include <JpegConverter.h>
#include <log.h>
#include <sys/stat.h>
#include <string>
#include <vector>
#include "gtest/gtest.h"

TEST(ConverterTest, Test_LoadSave) {
  std::string filename = "qr/qrcode.jpeg";
  std::string outfilename = "qr/out_qrcode.jpeg";
  int sideSize = 400;

  JpegConverter converter;
  const JpegConverter::ImageInfo* image = converter.Load(filename);
  ASSERT_TRUE(image != NULL);
  ASSERT_TRUE(image->data != NULL);
  ASSERT_EQ(sideSize, image->width);
  ASSERT_EQ(sideSize, image->height);
  ASSERT_NO_THROW(converter.Save(outfilename));

  struct stat statBuf;
  ::stat(filename.c_str(), &statBuf);
  ASSERT_GT(statBuf.st_size, 0);
}

#include <SocketClient.h>
#include <SocketException.h>
#include <SocketServer.h>
#include <log.h>
#include <unistd.h>
#include <chrono>
#include <cstdlib>
#include <future>
#include <thread>
#include "gtest/gtest.h"

namespace {
const int PYTHON_PORT = 8888;
int SERVER_PORT = 9000;  // Will be incremented for each test to avoid testing
                         // on recently used ports

const char* SERVER_IP = "localhost";

const int SHORT_TIMEOUT_MS = 100;
const int POLL_TIMEOUT_MS = 1 * 1000;
}

TEST(SocketTest, Test_ServerCDtor) { SocketServer server; }

TEST(SocketTest, Test_ClientCDtor) { SocketClient client; }

TEST(SocketTest, Test_Server) {
  SERVER_PORT++;
  SocketServer socket;
  ASSERT_TRUE(socket.Bind(SERVER_PORT));
  ASSERT_TRUE(socket.IsValid());

  SocketServer duplicate;
  ASSERT_FALSE(duplicate.Bind(SERVER_PORT));
}

TEST(SocketTest, Test_BlankPoll) {
  SERVER_PORT++;
  SocketServer server;
  ASSERT_TRUE(server.Bind(SERVER_PORT));
  ASSERT_TRUE(server.Poll(SHORT_TIMEOUT_MS));
}

bool PollSocket(int pollCount) {
  bool result = true;
  SocketServer server;
  if (!server.Bind(SERVER_PORT)) {
    return false;
  }
  for (int i = 0; i < pollCount; ++i) {
    result = result && server.Poll(POLL_TIMEOUT_MS);
  }

  return result;
}

TEST(SocketTest, Test_Connect) {
  SERVER_PORT++;
  std::future<bool> pollResult = std::async(std::launch::async, &PollSocket, 1);

  std::this_thread::sleep_for(std::chrono::milliseconds(100));  // Give server time to set up
  SocketClient client;
  ASSERT_TRUE(client.Connect(SERVER_IP, SERVER_PORT));
  ASSERT_TRUE(pollResult.get());
}

TEST(SocketTest, Test_Send) {
  SERVER_PORT++;
  std::future<bool> pollResult = std::async(std::launch::async, &PollSocket, 2);

  std::this_thread::sleep_for(std::chrono::milliseconds(100));  // Give server time to set up
  SocketClient client;
  ASSERT_TRUE(client.Connect(SERVER_IP, SERVER_PORT));
  ASSERT_TRUE(client.Send("Nobody expects the Spanish Inquisition!"));

  // Append Data
  for (int i = 0; i < 10; ++i) {
    ASSERT_TRUE(client.Send(" " + std::to_string(i)));
  }

  ASSERT_FALSE(client.Send(""));

  std::string largeStr(2048, ' ');
  ASSERT_TRUE(client.Send(largeStr));

  SocketClient dummy;
  ASSERT_FALSE(dummy.Send("Should have connected first"));

  ASSERT_TRUE(pollResult.get());
}

#include <BufferConverter.h>
#include <Decoder.h>
#include <JpegConverter.h>
#include <log.h>
#include <math.h>
#include <string>
#include <vector>
#include "gtest/gtest.h"

std::string PrintBuffer(uint8_t* buffer, size_t width, size_t height) {
  std::stringstream ss;
  for (size_t i(0); i < width * height; ++i) {
    if (i % width == 0) {
      ss << "\n";
    }
    ss << std::to_string(buffer[i]) << " ";
  }
  return ss.str();
}

void SaveImage(uint8_t* buffer, size_t width, size_t height, const std::string& filename) {
  JpegConverter saver;
  JpegConverter::ImageInfo image;
  image.width = width;
  image.height = height;
  image.numComponents = 1;
  image.data = buffer;

  saver.SetImage(&image);
  saver.Save(filename);
}

TEST(ProcessingTest, Test_ScaleBuffer) {
  uint8_t buffer[4] = {0, 1, 2, 3};
  const size_t sideSize = 2;
  const uint factor = 2;
  size_t scaledSideSize = sideSize * factor;
  size_t scaledSize = scaledSideSize * scaledSideSize;

  BufferConverter converter;
  ASSERT_EQ(scaledSize, converter.GetScaledSize(sideSize, sideSize, factor));
  std::vector<uint8_t> scaledBuffer(scaledSize);

  BufferConverter::ChangeData scaleChange;
  scaleChange.width = sideSize;
  scaleChange.height = sideSize;
  scaleChange.buffer = buffer;
  scaleChange.outbuffer = &scaledBuffer[0];

  converter.ScaleBuffer(&scaleChange, factor);

  std::string actual = PrintBuffer(&scaledBuffer[0], scaledSideSize, scaledSideSize);
  std::string expected =
      "\n0 0 1 1 "
      "\n0 0 1 1 "
      "\n2 2 3 3 "
      "\n2 2 3 3 ";

  LOG(INFO) << actual;
  ASSERT_EQ(expected, actual);
}

TEST(ProcessingTest, Test_PadBuffer) {
  uint8_t buffer[4] = {0, 1, 2, 3};
  const size_t sideSize = 2;
  uint padding = 1;
  size_t paddedSideSize = sideSize + 2 * padding;
  size_t paddedSize = 16;

  BufferConverter converter;
  ASSERT_EQ(paddedSize, converter.GetPaddedSize(sideSize, sideSize, padding));
  std::vector<uint8_t> paddedBuffer(paddedSize);

  BufferConverter::ChangeData paddingChange;
  paddingChange.width = sideSize;
  paddingChange.height = sideSize;
  paddingChange.buffer = buffer;
  paddingChange.outbuffer = &paddedBuffer[0];

  converter.PadBuffer(&paddingChange, padding, 0);

  std::string actual = PrintBuffer(&paddedBuffer[0], paddedSideSize, paddedSideSize);
  std::string expected =
      "\n0 0 0 0 "
      "\n0 0 1 0 "
      "\n0 2 3 0 "
      "\n0 0 0 0 ";

  LOG(INFO) << actual;
  ASSERT_EQ(expected, actual);
}

TEST(ProcessingTest, Test_ReadModifiedBuffer) {
  Decoder decoder;
  JpegConverter loader;
  BufferConverter converter;

  const std::string filename = "qr/qrcode_double_nopadding.jpeg";
  const std::string outPaddedName = "qr/out_qrcode_padded.jpeg";
  const std::string outModifiedName = "qr/out_qrcode_modified.jpeg";
  const std::string qrText = "Hello world!";

  const size_t sideSize = 42;
  const int factor = 10;
  const int padding = 1;

  // Calculate it
  size_t paddedSize = converter.GetPaddedSize(sideSize, sideSize, padding);
  size_t paddedSideSize = sideSize + (padding * 2);

  size_t scaledSize = converter.GetScaledSize(paddedSideSize, paddedSideSize, factor);
  size_t scaledSideSize = paddedSideSize * factor;

  // Load it
  const JpegConverter::ImageInfo* image = loader.Load(filename);
  ASSERT_TRUE(image != NULL);
  ASSERT_TRUE(image->data != NULL);

  // Check it
  ASSERT_EQ("", decoder.ReadBuffer(image->data, sideSize, sideSize));

  // Pad it
  std::vector<uint8_t> paddedBuffer(paddedSize);

  BufferConverter::ChangeData change;
  change.width = sideSize;
  change.height = sideSize;
  change.buffer = image->data;
  change.outbuffer = &paddedBuffer[0];

  converter.PadBuffer(&change, padding);

  // Save it
  SaveImage(&paddedBuffer[0], paddedSideSize, paddedSideSize, outPaddedName);

  // Scale it
  std::vector<uint8_t> scaledBuffer(scaledSize);

  change.width = paddedSideSize;
  change.height = paddedSideSize;
  change.buffer = change.outbuffer;
  change.outbuffer = &scaledBuffer[0];

  converter.ScaleBuffer(&change, factor);

  // Save it
  SaveImage(&scaledBuffer[0], scaledSideSize, scaledSideSize, outModifiedName);

  // Load it
  image = loader.Load(outModifiedName);
  ASSERT_TRUE(image != NULL);
  ASSERT_TRUE(image->data != NULL);

  // Assert it
  ASSERT_EQ(qrText, decoder.ReadBuffer(image->data, scaledSideSize, scaledSideSize));
}

#include <DataString.h>
#include <log.h>
#include "gtest/gtest.h"

TEST(DataStringTest, Test_ToString) {
  std::vector<uint8_t> input{0, 1, 2, 255};
  std::string encoded = DataString::ToString(&input[0], input.size());
  LOG(INFO) << "base64 data: " << encoded;

  std::vector<uint8_t> output = DataString::ToData(encoded);
  ASSERT_EQ(input, output);
}

#pragma once

#include <SocketChannel.h>
#include <map>
#include <memory>
#include <sstream>
#include <string>
#include <vector>

struct Client {
  std::string name;
  std::string address;
  int port;
  bool busy;
};

typedef std::map<std::string, std::shared_ptr<Client>> ClientMap_t;

class UIController : public IMessageListener {
 public:
  UIController();
  ~UIController();

  void AddPlotter(const std::string& name, const std::string& address, int port);
  void AddScanner(const std::string& name, const std::string& address, int port);

  void StartComplete();
  void StartPlot(const std::string& payload);
  void StartScan();
  void ListenFile(const std::string& filename) {}

 public:  // IMessageListener functions
  void OnMessage(const Message& message, MessageReply* reply);
  void OnError(const std::string& error) {}
  void OnConnect() {}
  void OnDisconnect() {}

 private:
  void Listen(std::istream* input) {}
  std::shared_ptr<Client> CreateClient(const std::string& name, const std::string& address, int port);
  void SendServerInfo(const std::string& name, const std::string& address, int port);
  std::shared_ptr<Client> FindReadyClient(const ClientMap_t& coll);
  std::string Decode(std::shared_ptr<std::stringstream> dataString, int width, int height);

 private:
  SocketChannel controlChannel;

  ClientMap_t plotters;
  ClientMap_t scanners;
  std::map<int, std::shared_ptr<std::stringstream>> scannedCodes;

 private:
  const char DELIM = ';';
};

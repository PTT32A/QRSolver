#include <InputParser.h>
#include <SocketChannel.h>
#include <UIController.h>
#include <log.h>
#include <stringsplit.h>
#include <memory>

INITIALIZE_EASYLOGGINGPP

namespace {
int portServer = 8501;
int portClient = 8500;

const char DELIM = ':';
const char* HELP_CMD("help");
const char* ADD_SCAN_CMD("scanner");
const char* ADD_PLOT_CMD("plotter");
const char* SCAN_CMD("startscan");
const char* PLOT_CMD("startplot");
const char* EXIT_CMD("exit");

std::shared_ptr<SocketChannel> serverChannel;
}  // namespace

void SigCatcher(int signum, siginfo_t* info, void* ptr) {
  LOG(INFO) << "caught " << strsignal(signum);
  serverChannel.reset();
  exit(0);
}

void ShowMenu() {
  std::cout << "Commands:" << std::endl
            << " - " << HELP_CMD << std::endl
            << " - " << ADD_SCAN_CMD << ":name:address" << std::endl
            << " - " << ADD_PLOT_CMD << ":name:address" << std::endl
            << " - " << SCAN_CMD << std::endl
            << " - " << PLOT_CMD << ":payload" << std::endl
            << " - " << EXIT_CMD << std::endl;
}

int main(int argc, char** argv) {
  el::Configurations conf("logger.conf");
  el::Loggers::reconfigureLogger("default", conf);

  // ensures SIGTERM and SIGINT are caught and handled properly
  struct sigaction action;
  memset(&action, 0, sizeof(struct sigaction));
  action.sa_sigaction = SigCatcher;
  action.sa_flags = SA_SIGINFO;
  sigaction(SIGTERM, &action, NULL);
  sigaction(SIGINT, &action, NULL);

  std::shared_ptr<UIController> controller = std::make_shared<UIController>();

  serverChannel = std::make_shared<SocketChannel>(portServer);
  serverChannel->Start();
  serverChannel->AddListener(controller);

  std::string input;
  while (input != "exit") {
    ShowMenu();
    std::getline(std::cin, input);
    auto split = stringsplit::split(input, DELIM);
    try {
      if (input.find(ADD_SCAN_CMD) == 0) {
        controller->AddScanner(split[1], split[2], portClient);
      } else if (input.find(ADD_PLOT_CMD) == 0) {
        controller->AddPlotter(split[1], split[2], portClient);
      } else if (input.find(SCAN_CMD) == 0) {
        controller->StartScan();
      } else if (input.find(PLOT_CMD) == 0) {
        controller->StartPlot(split[1]);
      }
    } catch (std::exception& ex) {
      LOG(ERROR) << "Error: " << ex.what();
    } catch (SocketException& ex) {
      LOG(ERROR) << "Socket Exception: " << ex.description();
    }
  }
  return 0;
}

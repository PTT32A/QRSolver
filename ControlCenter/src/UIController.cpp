#include <DataString.h>
#include <Decoder.h>
#include <JpegConverter.h>
#include <SSTR.h>
#include <UIController.h>
#include <log.h>
#include <stringsplit.h>

UIController::UIController() {}

UIController::~UIController() {}

void UIController::AddPlotter(const std::string& name, const std::string& address, int port) {
  if (plotters.find(name) != plotters.end()) {
    throw std::invalid_argument("Invalid name: a plotter with that name already exists");
  }
  SendServerInfo(name, address, port);
  plotters.emplace(name, CreateClient(name, address, port));
}

void UIController::AddScanner(const std::string& name, const std::string& address, int port) {
  if (scanners.find(name) != scanners.end()) {
    throw std::invalid_argument("Invalid name: a scanner with that name already exists");
  }
  SendServerInfo(name, address, port);
  scanners.emplace(name, CreateClient(name, address, port));
}

void UIController::StartComplete() { LOG(WARNING) << "Not yet implemented"; }

void UIController::StartPlot(const std::string& payload) {
  std::shared_ptr<Client> plotter = FindReadyClient(plotters);
  if (plotter != nullptr) {
    Message msg(SSTR("STARTPLOT" << DELIM << payload));
    controlChannel.Send(msg, plotter->address, plotter->port);
    plotter->busy = true;
  }
}

void UIController::StartScan() {
  std::shared_ptr<Client> scanner = FindReadyClient(scanners);
  if (scanner != nullptr) {
    Message msg("STARTSCAN");
    controlChannel.Send(msg, scanner->address, scanner->port);
    scanner->busy = true;
  }
}

void UIController::OnMessage(const Message& message, MessageReply* reply) {
  const std::string content = message.GetContent();
  AddressInfo msgAddress = message.GetSenderAddress();
  auto split = stringsplit::split(content, DELIM);

  auto it = scannedCodes.find(msgAddress.fDesc);
  if (it != scannedCodes.end()) {
    if (content.find("STOPSEND") == 0) {
      std::string name = split[1];
      int width = std::stoi(split[2]);
      int height = std::stoi(split[3]);
      int fd = msgAddress.fDesc;
      LOG(INFO) << "Decoded message: " << Decode(scannedCodes[fd], width, height);
      scannedCodes.erase(fd);
    } else {
      *(it->second) << content;
    }

    return;
  }

  LOG(INFO) << content;

  try {
    if (content.find("SCANNER READY") == 0) {
      std::string name = split[1];
      scanners[name]->busy = false;
      LOG(INFO) << "scanner ready: " << name << " on " << msgAddress.address;
    } else if (content.find("PLOTTER READY") == 0) {
      std::string name = split[1];
      plotters[name]->busy = false;
      LOG(INFO) << "plotter ready: " << name << " on " << msgAddress.address;
    } else if (content.find("SCANNER SHUTDOWN") == 0) {
      std::string name = split[1];
      scanners.erase(name);
    } else if (content.find("PLOTTER SHUTDOWN") == 0) {
      std::string name = split[1];
      plotters.erase(name);
    } else if (content.find("PLOTTER DONE") == 0) {
      std::string name = split[1];
      plotters[name]->busy = false;
    } else if (content.find("STARTSEND") == 0) {
      std::string name = split[1];
      int fd = msgAddress.fDesc;
      scannedCodes.emplace(fd, std::make_shared<std::stringstream>());
    } else {
      LOG(WARNING) << "Message went unhandled";
    }
  } catch (std::exception& ex) {
    LOG(ERROR) << "Exception while parsing message: " << ex.what();
  } catch (SocketException& ex) {
    LOG(ERROR) << "Socket Exception while parsing message: " << ex.description();
  }

  // na info -> PLOTTER/SCANNER READY
  // na STOP -> PLOTTER/SCANNER SHUTDOWN
  // na STARTPLOT -> als klaar -> PLOTTER DONE
  // na STARTSCAN -> na STARTSEND alle data die binnen komt loggen tot STOPSEND

  // default convention is <COMMAND>;<NAME>
  // names are set in the SendServerInfo() call
  // exception is STOPSEND, which is STOPSEND;<NAME>;<WIDTH>;<HEIGHT>
}

std::shared_ptr<Client> UIController::CreateClient(const std::string& name, const std::string& address, int port) {
  std::shared_ptr<Client> client = std::make_shared<Client>();
  client->name = name;
  client->address = address;
  client->port = port;
  client->busy = true;
  return client;
}

void UIController::SendServerInfo(const std::string& name, const std::string& address, int port) {
  try {
    Message msg(SSTR("hello" << DELIM << name));
    controlChannel.Send(msg, address, port);
  } catch (std::exception& ex) {
    LOG(ERROR) << "Unable to contact " << name << " at " << address << ":" << port;
  }
}

std::shared_ptr<Client> UIController::FindReadyClient(const ClientMap_t& coll) {
  for (auto pair : coll) {
    if (!pair.second->busy) {
      return pair.second;
    }
  }
  LOG(WARNING) << "No available client found";
  return nullptr;
}

std::string UIController::Decode(std::shared_ptr<std::stringstream> dataString, int width, int height) {
  LOG(INFO) << "Decoding image... (scanned_code.jpeg)";
  auto data = DataString::ToData(dataString->str());
  dataString.reset();  // dump some memory
  JpegConverter::ImageInfo inputInfo;
  inputInfo.width = width;
  inputInfo.height = height;
  inputInfo.numComponents = 1;
  inputInfo.data = &data[0];

  JpegConverter converter;
  converter.SetImage(&inputInfo);
  converter.Save("scanned_code.jpeg");

  // dump that memory
  data.clear();
  converter.SetImage(NULL);

  Decoder decoder;
  std::string content = decoder.ReadJpeg("scanned_code.jpeg");
  return content;
}

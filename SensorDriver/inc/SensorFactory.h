#pragma once
#include <memory>
#include <string>
#include "ColorSensor.h"
class SensorFactory {
 public:
  std::shared_ptr<ISensor> CreateSensor();
};

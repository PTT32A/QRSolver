#pragma once
#include <string>
#include "ISensor.h"
class ColorSensorStub : public ISensor {
 public:
  // reflected light intensity 0 - 100
  int GetValue(unsigned int index) { return 30; }
  // possible modes see
  // http://www.ev3dev.org/docs/sensors/lego-ev3-color-sensor/
  void SetMode(std::string mode) { throw "not possible in stub"; }
  std::string GetUnits() { return "light intensity"; }
  const void* GetRawData() { return (int*)1000; }
  std::string GetAddress() { return "stub"; }
  std::string GetMode() { return "COL-REFLECT"; }
  discreteValue GetDiscreteValue() { return White; }
};

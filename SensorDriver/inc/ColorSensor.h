#pragma once
#include <ev3dev.h>
#include <string>
#include "ISensor.h"

class ColorSensor : public ISensor {
 public:
  explicit ColorSensor(std::string port);
  // value 1 is black, 6 is white
  int GetValue(unsigned int index);
  // possible modes see
  // http://www.ev3dev.org/docs/sensors/lego-ev3-color-sensor/
  void SetMode(std::string mode);
  std::string GetUnits();
  const void* GetRawData();
  std::string GetAddress();
  std::string GetMode();
  bool ColorIsWhite();
  discreteValue GetDiscreteValue();

 private:
  ev3dev::color_sensor Sensor;
};

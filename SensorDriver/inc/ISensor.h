#pragma once
#include <string>
enum discreteValue { Black = 0, White = 255 };
class ISensor {
 public:
  virtual int GetValue(unsigned int index) = 0;
  // virtual int SetPollTime(int periodMs) = 0; most ev3 sensors can't do this!
  virtual void SetMode(std::string mode) = 0;
  virtual std::string GetMode() = 0;
  virtual std::string GetUnits() = 0;
  virtual const void* GetRawData() = 0;
  virtual std::string GetAddress() = 0;
  virtual discreteValue GetDiscreteValue() = 0;
};

#include "ColorSensor.h"
ColorSensor::ColorSensor(std::string port) : Sensor(port) { SetMode("COL-REFLECT"); }

int ColorSensor::GetValue(unsigned int index) { return Sensor.value(index); }
void ColorSensor::SetMode(std::string mode) { Sensor.set_mode(mode); }
std::string ColorSensor::GetMode() { return (std::string)Sensor.mode(); }
std::string ColorSensor::GetUnits() { return Sensor.units(); }
const void* ColorSensor::GetRawData() {
  return &Sensor.bin_data().back();  // last value from vector
}
std::string ColorSensor::GetAddress() { return Sensor.address(); }
discreteValue ColorSensor::GetDiscreteValue() { return (GetValue(0) > 50) ? White : Black; }

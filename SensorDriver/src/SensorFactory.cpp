#include <ev3dev.h>
#include <log.h>
#include <fstream>
#include <iostream>
#include "ColorSensorStub.h"
#include "SensorFactory.h"

std::shared_ptr<ISensor> SensorFactory::CreateSensor() {
  std::fstream fs;
  fs.open("sensor.conf", std::ios::in);
  std::string port;
  fs >> port;
  LOG(INFO) << "sensor on port: " << port;
  fs.close();
  if (port == "in1") {
    return std::make_shared<ColorSensor>(ColorSensor(port));
  }
  if (port == "in2") {
    throw "not implemented";
  }
  if (port == "in3") {
    throw "not implemented";
  }
  if (port == "in4") {
    throw "not implemented";
  }
  if (port == "stub") {
    std::cout << "using stub!" << std::endl;
    return std::make_shared<ColorSensorStub>(ColorSensorStub());
  }
  throw "invalid argument!";
}

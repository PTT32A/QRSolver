#include <SocketChannel.h>
#include <log.h>
#include <iostream>
#include <memory>
#include <string>
#include <vector>
#include "CommunicationDemo.h"
#include "DemoListener.h"

namespace {
// Linting suggests to use char*, while std::string member functions are used
const std::string HELP_CMD("help");       // NOLINT
const std::string PORT_CMD(">port ");     // NOLINT
const std::string ADDR_CMD(">address ");  // NOLINT
const std::string SEND_CMD(">send ");     // NOLINT
const std::string RPSEND_CMD("send");     // NOLINT
const std::string EXIT_CMD("exit");       // NOLINT
}

void CommunicationDemo::Start(InputParser* opts) {
  int localPort = std::stoi(opts->getCmdOption("-p"));
  SocketChannel channel(localPort);
  std::shared_ptr<DemoListener> listener = std::make_shared<DemoListener>();
  channel.AddListener(listener);
  channel.Start();

  std::cout << "Welcome to the Communication Demo" << std::endl;
  PrintCommands();

  std::string input;
  while (true) {
    std::getline(std::cin, input);
    if (input == HELP_CMD) {
      PrintCommands();
    } else if (input.find(PORT_CMD) == 0) {
      SetPort(input.substr(PORT_CMD.length()));
    } else if (input.find(ADDR_CMD) == 0) {
      address = input.substr(ADDR_CMD.length());
    } else if (input.find(SEND_CMD) == 0) {
      Send(&channel, input.substr(SEND_CMD.length()));
    } else if (input == RPSEND_CMD) {
      LoopSend(&channel);
    } else if (input == EXIT_CMD) {
      break;
    } else {
      std::cout << "Syntax error" << std::endl;
      PrintCommands();
    }
  }
}

void CommunicationDemo::SetPort(const std::string& newPort) {
  try {
    port = std::stoi(newPort);
  } catch (std::exception& ex) {
    LOG(ERROR) << "Unable to set port to " << newPort;
  }
}

void CommunicationDemo::Send(SocketChannel* channel,
                             const std::string& message) {
  try {
    channel->Send(Message(message), address, port);
  } catch (SocketException& ex) {
    LOG(ERROR) << "SocketException: " << ex.description();
  } catch (std::exception& ex) {
    LOG(ERROR) << "Exception: " << ex.what();
  }
}

void CommunicationDemo::LoopSend(SocketChannel* channel) {
  std::string input;
  std::cout << "What would you like to send?" << std::endl
            << "Type EOF to exit" << std::endl;
  while (true) {
    std::getline(std::cin, input);
    if (input == "EOF") {
      break;
    }
    Send(channel, input);
  }
  std::cout << "Returning to main menu..." << std::endl;
  PrintCommands();
}

void CommunicationDemo::PrintCommands() {
  std::cout << "Commands:" << std::endl
            << " - '" << PORT_CMD << "(value)'" << std::endl
            << " - '" << ADDR_CMD << "(value)'" << std::endl
            << " - '" << SEND_CMD << "(value)'" << std::endl
            << " - '" << RPSEND_CMD << "'" << std::endl
            << " - '" << HELP_CMD << "'" << std::endl
            << " - '" << EXIT_CMD << "'" << std::endl;
}

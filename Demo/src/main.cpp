#include <CommunicationDemo.h>
#include <MotorDriverDemo.h>
#include <MotionDemo.h>
#include <InputParser.h>
#include <log.h>

INITIALIZE_EASYLOGGINGPP

int main(int argc, char **argv) {
  InputParser input(argc, argv);

  if (input.cmdOptionExists("-communication") || input.cmdOptionExists("-comm")) {
    CommunicationDemo demo;
    demo.Start(&input);
    return 0;
  }
  if (input.cmdOptionExists("-motion")) {
    MotionDemo demo;
    demo.Start();
    return 0;
  }
  if (input.cmdOptionExists("-motordriver") || input.cmdOptionExists("-motor")) {
    MotorDriverDemo demo;
    demo.Start(&input);
    return 0;
  }
  if (input.cmdOptionExists("-scan")) {
    // ScanClientDemo demo;
    // demo.Start();
    return 0;
  }

  LOG(ERROR) << "Unknown demo type argument ";
  return -1;
}

#include <Motioncontroller.h>
#include <log.h>
#include <stringsplit.h>
#include <iostream>
#include <memory>
#include <string>
#include "MotionDemo.h"

namespace {
// Linting suggests to use char*, while std::string member functions are used
const std::string HELP_CMD("help");    // NOLINT
const std::string MABS_CMD(">abs ");   // NOLINT
const std::string MREL_CMD(">rel ");   // NOLINT
const std::string HOME_CMD(">home");   // NOLINT
const std::string GPOSX_CMD(">posx");  // NOLINT
const std::string GPOSY_CMD(">posy");  // NOLINT
const std::string GPOSZ_CMD(">posz");  // NOLINT
const std::string EXIT_CMD("exit");    // NOLINT
}  // namespace

void MotionDemo::Start() {
  Motioncontroller controller;

  std::cout << "Welcome to the Motion Demo" << std::endl;
  PrintCommands();

  std::string input;
  std::vector<std::string> ssplit;

  while (true) {
    try {
      ssplit.clear();
      std::getline(std::cin, input);
      LOG(INFO) << "input == " << input;
      if (input == HELP_CMD) {
        PrintCommands();
      } else if (input.find(MABS_CMD) == 0) {
        ssplit = stringsplit::split(input, ' ');
        double valueX = atof(ssplit[1].c_str());  // value from X
        double valueY = atof(ssplit[2].c_str());  // value from Y
        double valueZ = atof(ssplit[3].c_str());  // value from Z
        controller.AbsolutePtP(valueX, valueY, valueZ);

      } else if (input.find(MREL_CMD) == 0) {
        ssplit = stringsplit::split(input, ' ');
        double valueX = atof(ssplit[1].c_str());  // value from X
        double valueY = atof(ssplit[2].c_str());  // value from Y
        double valueZ = atof(ssplit[3].c_str());  // value from Z
        LOG(INFO) << "demo command relative ptp to " << valueX << ", " << valueY << ", " << valueZ;
        controller.RelativePtP(valueX, valueY, valueZ);

      } else if (input == HOME_CMD) {
        controller.Home();
      } else if (input == GPOSX_CMD) {
        std::cout << "X Pos Value: " << controller.GetXPos() << std::endl;
      } else if (input == GPOSY_CMD) {
        std::cout << "Y Pos Value: " << controller.GetYPos() << std::endl;
      } else if (input == GPOSZ_CMD) {
        std::cout << "Z Pos Value: " << controller.GetZPos() << std::endl;
      } else if (input == EXIT_CMD) {
        break;
      } else {
        std::cout << "Syntax error" << std::endl;
        PrintCommands();
      }
    } catch (std::exception& ex) {
      LOG(ERROR) << ex.what();
    }
  }
}

void MotionDemo::PrintCommands() {
  std::cout << "Commands:" << std::endl
            << " - '" << MABS_CMD << " (value x) (value Y) (value Z)'" << std::endl
            << " - '" << MREL_CMD << " (value x)|(value Y)|(value Z)'" << std::endl
            << " - '" << HOME_CMD << "'" << std::endl
            << " - '" << GPOSX_CMD << "'" << std::endl
            << " - '" << GPOSY_CMD << "'" << std::endl
            << " - '" << GPOSZ_CMD << "'" << std::endl
            << " - '" << EXIT_CMD << "'" << std::endl;
}

#include <MotorFactory.h>
#include <iostream>
#include "MotorDriverDemo.h"

namespace {
// Linting suggests to use char*, while std::string member functions are used
const std::string HELP_CMD("help");           // NOLINT
const std::string SETMOTOR_CMD("setmotor ");  // NOLINT
const std::string STOPMOTOR_CMD("stop");      // NOLINT
const std::string MOVEREL_CMD("moverel ");    // NOLINT
const std::string MOVEABS_CMD("moveabs ");    // NOLINT
const std::string GETPOS_CMD("getpos");       // NOLINT
const std::string GETSPEED_CMD("getspeed");   // NOLINT
const std::string HOME_CMD("home");           // NOLINT
}  // namespace

void MotorDriverDemo::Start(InputParser *opts) {
  std::cout << "Welcome to the MotorDriver Demo" << std::endl;
  PrintCommands();

  std::string input;
  while (true) {
    std::getline(std::cin, input);
    if (input == HELP_CMD) {
      PrintCommands();
    } else if (input.find(SETMOTOR_CMD) == 0) {
      SetMotor(input.substr(SETMOTOR_CMD.length()));
    } else if (input.find(STOPMOTOR_CMD) == 0) {
      Stop();
    } else if (input.find(MOVEREL_CMD) == 0) {
      MoveRel(input.substr(MOVEREL_CMD.length()));
    } else if (input.find(MOVEABS_CMD) == 0) {
      MoveAbs(input.substr(MOVEABS_CMD.length()));
    } else if (input.find(GETPOS_CMD) == 0) {
      GetPos();
    } else if (input.find(GETSPEED_CMD) == 0) {
      GetSpeed();
    } else if (input.find(HOME_CMD) == 0) {
      Home();
    } else {
      std::cout << "Syntax error" << std::endl;
      PrintCommands();
    }
  }
}

void MotorDriverDemo::SetMotor(const std::string &motor) {
  MotorFactory factory;
  if (motor == "x") {
    Motor = factory.CreateTachoMotor(MOTOR_X);
  } else if (motor == "y") {
    Motor = factory.CreateTachoMotor(MOTOR_Y);
  } else if (motor == "z") {
    Motor = factory.CreateTachoMotor(MOTOR_Z);
  } else {
    std::cout << "Invalid motor given" << std::endl;
  }
}

void MotorDriverDemo::Stop() { Motor->Stop(); }

void MotorDriverDemo::MoveRel(const std::string &position) {
  Motor->MoveRel(std::stoi(position));
}

void MotorDriverDemo::MoveAbs(const std::string &position) {
  Motor->MoveAbs(std::stoi(position));
}

void MotorDriverDemo::GetPos() { std::cout << Motor->GetPos() << std::endl; }

void MotorDriverDemo::GetSpeed() {
  std::cout << Motor->GetSpeed() << std::endl;
}

void MotorDriverDemo::Home() { Motor->Home(); }

void MotorDriverDemo::PrintCommands() {
  std::cout << "Commands:" << std::endl
            << " - '" << SETMOTOR_CMD << "(x,y,z)'" << std::endl
            << " - '" << STOPMOTOR_CMD << std::endl
            << " - '" << MOVEREL_CMD << "(value)'" << std::endl
            << " - '" << MOVEABS_CMD << "(value)'" << std::endl
            << " - '" << GETPOS_CMD << std::endl
            << " - '" << GETSPEED_CMD << std::endl
            << " - '" << HOME_CMD << std::endl;
}

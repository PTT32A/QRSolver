#pragma once

#include <InputParser.h>
#include <SocketChannel.h>
#include <string>

class CommunicationDemo {
 public:
  void Start(InputParser* input);

 private:
  void PrintCommands();
  void LoopSend(SocketChannel* channel);
  void Send(SocketChannel* channel, const std::string& message);
  void SetPort(const std::string& newPort);

 private:
  int port;
  std::string address;
};

#pragma once

#include <IMessageListener.h>
#include <string>

class DemoListener : public IMessageListener {
 public:
  void OnMessage(const Message& message, MessageReply* reply) {
    LOG(INFO) << "Message Received: " << message.GetContent();
  }
  void OnError(const std::string& error) {}
  void OnConnect() {}
  void OnDisconnect() {}
};

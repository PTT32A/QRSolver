#pragma once

#include <IMotorControl.h>
#include <InputParser.h>
#include <memory>
#include <string>

class MotorDriverDemo {
 public:
  void Start(InputParser *input);

 private:
  void PrintCommands();
  void SetMotor(const std::string &motor);

  void Stop();
  void MoveRel(const std::string &position);
  void MoveAbs(const std::string &position);
  void GetPos();
  void GetSpeed();
  void Home();

 private:
  std::shared_ptr<IMotorControl> Motor;
};

#include <ev3dev.h>
#include "GenericMotor.h"
#include "MotorFactory.h"

namespace {
const int MOTOR_DEFAULT_SPEED = 500;
const int MOTOR_HOME_SPEED = 140;
const int MAX_X_POSITION = 3900;
const int MAX_Y_POSITION = 2750;
const int MAX_Z_POSITION = 600;
const int MIN_POSITION = 0;
const int RAMP_TIME = 0;  // 1 sec to get to speed, 1 sec to get to zero speed
}

std::shared_ptr<IMotorControl> MotorFactory::CreateTachoMotor(eMotorPort port) {
  switch (port) {
    case MOTOR_X:
      return std::make_shared<GenericMotor>(GenericMotor(ev3dev::OUTPUT_A, MOTOR_DEFAULT_SPEED, MOTOR_HOME_SPEED,
                                                         MAX_X_POSITION, MIN_POSITION, RAMP_TIME,
                                                         ev3dev::motor::polarity_normal));
    case MOTOR_Y:
      return std::make_shared<GenericMotor>(GenericMotor(ev3dev::OUTPUT_B, MOTOR_DEFAULT_SPEED, MOTOR_HOME_SPEED,
                                                         MAX_Y_POSITION, MIN_POSITION, RAMP_TIME,
                                                         ev3dev::motor::polarity_inversed));
    case MOTOR_Z:
      return std::make_shared<GenericMotor>(GenericMotor(ev3dev::OUTPUT_C, MOTOR_DEFAULT_SPEED, MOTOR_HOME_SPEED,
                                                         MAX_Z_POSITION, MIN_POSITION, RAMP_TIME,
                                                         ev3dev::motor::polarity_normal));
  }
  return NULL;
}

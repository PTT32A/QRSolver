#include <log.h>
#include <chrono>
#include <stdexcept>
#include <thread>
#include "GenericMotor.h"

GenericMotor::GenericMotor(std::string port, int motorDefaultSpeed, int motorHomeSpeed, int maxPosition,
                           int minPosition, int rampTime, std::string homePolarity)
    : ThisMotor(port),
      MotorDefaultSpeed(motorDefaultSpeed),
      MotorHomeSpeed(motorHomeSpeed),
      MaxPosition(maxPosition),
      MinPosition(minPosition),
      RampTime(rampTime),
      HomePolarity(homePolarity) {
  LOG(INFO) << "motor created on: " << port;
  Reset();
}

GenericMotor::~GenericMotor() {}

void GenericMotor::Reset() {
  Stop();  //  resetting also stops the motor, however coasting instead of
           //  holding will be applied when calling reset directly instead of
           //  stop.
  ThisMotor.reset();
  // Switch polarity to positive
  if (HomePolarity == ev3dev::motor::polarity_normal) {
    ThisMotor.set_polarity(ev3dev::motor::polarity_inversed);
  } else {
    ThisMotor.set_polarity(ev3dev::motor::polarity_normal);
  }
  SetSpeed(MotorDefaultSpeed);
  ThisMotor.set_stop_action(ev3dev::motor::stop_action_hold);
  ThisMotor.set_ramp_up_sp(RampTime);
  ThisMotor.set_ramp_down_sp(RampTime);
}

void GenericMotor::Stop() { ThisMotor.stop(); }

void GenericMotor::MoveRel(int position) { MoveAbs(GetPos() + position); }

void GenericMotor::MoveAbs(int position) {
  if (position > MaxPosition || position < MinPosition) {
    throw std::out_of_range("Target position out of range");
  }

  LOG(INFO) << "moving to position: " << position;
  ThisMotor.set_position_sp(position);
  ThisMotor.run_to_abs_pos();
}

int GenericMotor::GetPos() const { return ThisMotor.position(); }

int GenericMotor::GetSpeed() const { return MotorDefaultSpeed; }

void GenericMotor::SetSpeed(int newDefaultSpeed) {
  if (newDefaultSpeed > ThisMotor.max_speed() || newDefaultSpeed <= 0) {
    throw std::out_of_range("newDefaultSpeed out of range");
  }
  MotorDefaultSpeed = newDefaultSpeed;
  ThisMotor.set_speed_sp(newDefaultSpeed);
}

ev3dev::mode_set GenericMotor::GetState() const { return ThisMotor.state(); }

void GenericMotor::Home() {
  ThisMotor.set_polarity(HomePolarity);
  ThisMotor.set_speed_sp(MotorHomeSpeed);
  ThisMotor.run_forever();

  bool looping = true;

  LOG(INFO) << "start homing";
  while (looping) {
    if (GetState().count(ev3dev::motor::state_overloaded)) {
      looping = false;
    }
  }
  Reset();
  MoveRel(GetPos());  //  move back what we did jump by accident
}

void GenericMotor::WaitMoveCompleted() {
  while (!GetState().count(ev3dev::motor::state_holding)) {
    std::this_thread::sleep_for(std::chrono::milliseconds(5));
  }
}

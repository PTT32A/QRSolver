#pragma once

class IMotorControl {
 public:
  /*
  Homes axis: moves towards 0 until it collides with a border.
  The border becomes the new 0 coordinate.
  */
  virtual void Home() = 0;

  /*
  Starts a move to a position relative to current position. (current position + position)
  */
  virtual void MoveRel(int position) = 0;

  /*
  Starts a move to an absolute position (0 + position)
  */
  virtual void MoveAbs(int position) = 0;

  /*
  Stops movement for current Motor.
  Will correct movement to ensure it ends up at position when stop was called.
  */
  virtual void Stop() = 0;

  /*
  Blocks current thread until the motor has finished moving.
  */
  virtual void WaitMoveCompleted() = 0;

  /*
  Returns the speed at which the motor runs.
  This value is not dependent on whether the motor is currently moving.
  */
  virtual int GetSpeed() const = 0;

  /*
  Sets speed at which the motor runs.
  Changes will take effect immediately - even if motor is currently running.
  */
  virtual void SetSpeed(int speed) = 0;

  /*
  Returns absolute position of axis.
  Should only be called after axis is homed.
  */
  virtual int GetPos() const = 0;
};

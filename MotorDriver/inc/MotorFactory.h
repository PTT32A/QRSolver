#pragma once
#include <memory>
#include "IMotorControl.h"

enum eMotorPort { MOTOR_X, MOTOR_Y, MOTOR_Z };

class MotorFactory {
 public:
  std::shared_ptr<IMotorControl> CreateTachoMotor(eMotorPort port);
};

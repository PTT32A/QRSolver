#pragma once
#include <ev3dev.h>
#include <string>
#include "IMotorControl.h"

class GenericMotor : public IMotorControl {
 public:
  GenericMotor(std::string port, int motorDefaultSpeed, int motorHomeSpeed, int maxPosition, int minPosition,
               int rampTime, std::string homePolarity);
  virtual ~GenericMotor();

 public:  // IMotorControl functions
  void Stop();
  void MoveRel(int position);
  void MoveAbs(int position);
  int GetPos() const;
  int GetSpeed() const;
  void SetSpeed(int newDefaultSpeed);
  void Home();
  void WaitMoveCompleted();

 public:
  ev3dev::mode_set GetState() const;

 private:
  ev3dev::motor ThisMotor;
  int MotorDefaultSpeed;
  int MotorHomeSpeed;
  int MaxPosition;
  int MinPosition;
  int RampTime;              // see http://www.ev3dev.org/docs/drivers/tacho-motor-class/
  std::string HomePolarity;  //  which way to drive to get to coordinate 0

  void Reset();
};

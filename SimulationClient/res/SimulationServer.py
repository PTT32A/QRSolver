#!/usr/bin/env python
"""Simulation server for QRSolver project"""

import socket
import sys
import threading


def server(port):
    """socket server function"""
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    # Bind socket to local host and port
    try:
        sock.bind(('', port))
    except socket.error as msg:
        print 'Bind failed. Error Code : ' + str(msg[0]) + ' Message ' + msg[1]
        sys.exit()
    # Start listening on socket
    sock.listen(10)

    # now keep talking with the client
    while 1:
        # wait to accept a connection - blocking call
        conn, addr = sock.accept()
        print 'Connected with ' + addr[0] + ':' + str(addr[1])
        data = conn.recv(1024)
        if data:
            print data
            conn.send(data)

    sock.close()
    return


def main():
    """main function"""
    server_port = int(raw_input('Server port: '))
    thread = threading.Thread(target=server, args=(server_port,))
    thread.daemon = True
    thread.start()

    client_port = int(raw_input('Client port: '))
    clientsocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    clientsocket.connect(('localhost', client_port))

    while 1:
        msg = raw_input('Message: ')
        clientsocket.send(msg)

if __name__ == "__main__":
    main()

#pragma once

#include <SocketChannel.h>
#include <memory>
#include <string>

class Sim : public IMessageListener {
 public:
  virtual void Start() = 0;

 public:
  void SetChannel(std::shared_ptr<SocketChannel> channel) { myChannel = channel; }
  std::shared_ptr<SocketChannel> GetChannel() { return myChannel; }

 protected:
  std::shared_ptr<SocketChannel> myChannel;
  std::string serverAddress;
  int serverPort;
};

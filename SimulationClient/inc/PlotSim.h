#pragma once

#include <string>
#include "Sim.h"

class PlotSim : public Sim {
 public:
  void Start();

 public:  // IMessageListener
  void OnMessage(const Message& message, MessageReply* reply);
  void OnError(const std::string& error);
  void OnConnect();
  void OnDisconnect();
};

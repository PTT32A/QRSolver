#include <Decoder.h>
#include <JpegConverter.h>
#include <SocketChannel.h>
#include <stringsplit.h>
#include "ScanSim.h"

void ScanSim::Start() {}

void ScanSim::OnMessage(const Message& message, MessageReply* reply) {
  std::string content = message.GetContent();
  if (content.find("info;") == 0) {  // "info;address;port"
    std::vector<std::string> info = stringsplit::split(content, ';');
    serverAddress = info[1];
    serverPort = std::stoi(info[2]);
    myChannel->Send(Message("ack"), serverAddress, serverPort);
  }

  if (content.find("scan") == 0) {
    SendScan();
  }
}

void ScanSim::OnError(const std::string& error) {}
void ScanSim::OnConnect() {}
void ScanSim::OnDisconnect() {}

void ScanSim::SendScan() {
  Decoder decoder;
  std::string code = decoder.ReadJpeg("qrcode_double_nopadding.jpeg");
  myChannel->Send(Message(code), serverAddress, serverPort);
}

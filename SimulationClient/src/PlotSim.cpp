#include <JpegConverter.h>
#include <SocketChannel.h>
#include <log.h>
#include <stringsplit.h>
#include "PlotSim.h"

void PlotSim::Start() {}

void PlotSim::OnMessage(const Message& message, MessageReply* reply) {
  std::string content = message.GetContent();
  if (content.find("info;") == 0) {  // "info;<address>;<port>"
    std::vector<std::string> info = stringsplit::split(content, ';');
    serverAddress = info[1];
    serverPort = std::stoi(info[2]);
    myChannel->Send(Message("ack"), serverAddress, serverPort);
  }
}

void PlotSim::OnError(const std::string& error) {}
void PlotSim::OnConnect() {}
void PlotSim::OnDisconnect() {}

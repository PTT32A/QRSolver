#include <InputParser.h>
#include <log.h>
#include <memory>
#include "PlotSim.h"
#include "ScanSim.h"

INITIALIZE_EASYLOGGINGPP

struct SimChannel {
  std::shared_ptr<SocketChannel> channel;
  std::shared_ptr<Sim> sim;
};

void PrintHelp() {
  LOG(INFO) << "Supported commands: " << std::endl
            << "-s | --scanner <port>        starts simulation scanner" << std::endl
            << "-p | --plotter <port>        starts simulation plotter" << std::endl
            << "-h | --help                  prints this help message";
}

void StartScanner(std::vector<SimChannel>* sims, int port) {
  LOG(INFO) << "Starting scanner on port " << port;
  SimChannel sim;
  sim.sim = std::make_shared<ScanSim>();
  sim.channel = std::make_shared<SocketChannel>(port);
  sim.channel->AddListener(sim.sim);
  sim.channel->Start();
  sim.sim->SetChannel(sim.channel);
  sims->push_back(sim);
}

void StartPlotter(std::vector<SimChannel>* sims, int port) {
  LOG(INFO) << "Starting plotter on port " << port;
  SimChannel sim;
  sim.sim = std::make_shared<PlotSim>();
  sim.channel = std::make_shared<SocketChannel>(port);
  sim.channel->AddListener(sim.sim);
  sim.channel->Start();
  sim.sim->SetChannel(sim.channel);
  sims->push_back(sim);
}

int main(int argc, char** argv) {
  el::Configurations conf("logger.conf");
  el::Loggers::reconfigureLogger("default", conf);

  InputParser opts(argc, argv);
  bool running = false;
  std::vector<SimChannel> sims;

  if (opts.cmdOptionExists("-h") || opts.cmdOptionExists("--help")) {
    PrintHelp();
    return 0;
  }

  if (opts.cmdOptionExists("-s")) {
    running = true;
    int localPort = std::stoi(opts.getCmdOption("-s"));
    StartScanner(&sims, localPort);
  }

  if (opts.cmdOptionExists("--scanner")) {
    running = true;
    int localPort = std::stoi(opts.getCmdOption("--scanner"));
    StartScanner(&sims, localPort);
  }

  if (opts.cmdOptionExists("-p")) {
    running = true;
    int localPort = std::stoi(opts.getCmdOption("-p"));
    StartPlotter(&sims, localPort);
  }

  if (opts.cmdOptionExists("--plotter")) {
    running = true;
    int localPort = std::stoi(opts.getCmdOption("--plotter"));
    StartPlotter(&sims, localPort);
  }

  if (!running) {
    LOG(ERROR) << "invalid arguments";
    PrintHelp();
    return -1;
  }

  std::string input;
  LOG(INFO) << "type 'exit' to leave simulation";
  while (input != "exit") {
    std::getline(std::cin, input);
  }
  return 0;
}

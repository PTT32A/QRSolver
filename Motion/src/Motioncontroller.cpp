#include <IMotorControl.h>
#include <log.h>
#include "Motioncontroller.h"

Motioncontroller::Motioncontroller() {
  MotorFactory motorfactory;

  motorX = motorfactory.CreateTachoMotor(MOTOR_X);
  motorY = motorfactory.CreateTachoMotor(MOTOR_Y);
  motorZ = motorfactory.CreateTachoMotor(MOTOR_Z);
}

Motioncontroller::~Motioncontroller() {}

void Motioncontroller::AbsolutePtP(double x, double y, double z) {
  AbsolutePtPAsync(x, y, z);
  WaitMoveCompleted();
}

void Motioncontroller::RelativePtP(double x, double y, double z) {
  RelativePtPAsync(x, y, z);
  WaitMoveCompleted();
}

void Motioncontroller::AbsolutePtPAsync(double x, double y, double z) {
  motorX->MoveAbs(x);
  motorY->MoveAbs(y);
  motorZ->MoveAbs(z);
}

void Motioncontroller::RelativePtPAsync(double x, double y, double z) {
  motorX->MoveRel(x);
  motorY->MoveRel(y);
  motorZ->MoveRel(z);
}

void Motioncontroller::WaitMoveCompleted() {
  motorX->WaitMoveCompleted();
  motorY->WaitMoveCompleted();
  motorZ->WaitMoveCompleted();
}

void Motioncontroller::Home() {
  motorZ->Home();
  motorX->Home();
  motorY->Home();
}

double Motioncontroller::GetXPos() { return motorX->GetPos(); }

double Motioncontroller::GetYPos() { return motorY->GetPos(); }

double Motioncontroller::GetZPos() { return motorZ->GetPos(); }

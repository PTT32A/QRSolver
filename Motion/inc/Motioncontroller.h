#pragma once

#include <MotorFactory.h>
#include "IAxisMovement.h"

class Motioncontroller : public IAxisMovement {
 public:
  void AbsolutePtP(double x, double y, double z);
  void RelativePtP(double x, double y, double z);
  void AbsolutePtPAsync(double x, double y, double z);
  void RelativePtPAsync(double x, double y, double z);
  void WaitMoveCompleted();
  void Home();
  double GetXPos();
  double GetYPos();
  double GetZPos();
  Motioncontroller();
  ~Motioncontroller();

 private:
  std::shared_ptr<IMotorControl> motorX;  // A
  std::shared_ptr<IMotorControl> motorY;  // B
  std::shared_ptr<IMotorControl> motorZ;  // C
};

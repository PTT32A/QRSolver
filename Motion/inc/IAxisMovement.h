#pragma once

class IAxisMovement {
 public:
  /*
  Moves Robot to specified XYZ coordinates.

  Example: AbsolutePtP(30, 20, 10) is called while robot is at coordinates x = 20, y = 20, z = 20.
  After the call returns, the robot will be at x = 30, y = 20, z = 10.
  */
  virtual void AbsolutePtP(double x, double y, double z) = 0;

  /*
  Moves Robot to specified XYZ coordinates - relative to current position.

  Example: RelativePtP(30, 20, 10) is called while robot is at coordinates x = 20, y = 20, z = 20.
  After the call returns, the robot will be at x = 50, y = 40, z = 30.
*/
  virtual void RelativePtP(double x, double y, double z) = 0;

  /*
  Starts moving Robot to specified absolute XYZ coordinates.
  Call returns immediately after initiating the move.

  New PtP calls made before the Robot stopped moving will not wait until the move action is completed.
  */
  virtual void AbsolutePtPAsync(double x, double y, double z) = 0;

  /*
  Starts moving Robot to specified relative XYZ coordinates.
  Call returns immediately after initiating the move.

  New PtP calls made before the Robot stopped moving will not wait until the move action is completed.
  */
  virtual void RelativePtPAsync(double x, double y, double z) = 0;

  /*
  Waits until current PtP move is completed.
  Returns immediately if no PtP commands are currently in progress.
  */
  virtual void WaitMoveCompleted() = 0;

  /*
  Starts homing all axes. This will find 0 coordinates for all axes.

  Pre: Robot is in undetermined position
  Post: Robot is placed at x = 0, y = 0, z = 0. PtP moves are now available.
  */
  virtual void Home() = 0;

  /*
  Returns current absolute position of the Robot X axis.
  */
  virtual double GetXPos() = 0;

  /*
  Returns current absolute position of the Robot Y axis.
  */
  virtual double GetYPos() = 0;

  /*
  Returns current absolute position of the Robot Z axis.
  */
  virtual double GetZPos() = 0;
};

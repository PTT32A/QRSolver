NORMAL      := 
CROSS       := arm-linux-gnueabi-

MODULES := Utils \
           Communication \
           SensorDriver \
           MotorDriver \
           Motion \
           Processing \
           ScanClient \
           PlotClient \
           ScanClientStub
TEST    := $(MODULES) Test
DEMO    := $(MODULES) Demo
ALL     := $(MODULES) ControlCenter Test Demo SimulationClient

.PHONY: setup setup-arm $(ALL) all

all: CXXTYPE=$(NORMAL)
all: $(ALL)

arm: CXXTYPE=$(CROSS)
arm: $(DEMO)

clean: TARGET=clean
clean: $(ALL)

cleaner: TARGET=cleaner
cleaner: $(ALL)

test: all
	@cd Test/bin && ./Test

demo: all
	@cd Demo/bin && ./Demo

install: arm
	scp Demo/bin/Demo robot@ev3dev.local:~/QRSolver

deploy:
	scp Demo/bin/Demo robot@ev3dev.local:~/QRDemo
	scp ScanClient/bin/ScanClient robot@ev3dev.local:~/QRScanner
	scp PlotClient/bin/PlotClient robot@ev3dev.local:~/QRPlotter
	scp ScanClientStub/bin/ScanClientStub robot@ev3dev.local:~/QRScannerStub

$(ALL):
	@echo "Making ***$@***"
	@cd $@ && make --no-print-directory $(TARGET) CXXTYPE=${CXXTYPE}

quirc-clean:
	@make -C quirc clean

quirc-cleaner: quirc-clean
	@make -C quirc uninstall

submodules:
	@git submodule update --init

# requires sudo
setup:
	@git submodule update --init
	@make -C quirc install
	@make quirc-clean

# requires sudo
setup-arm:
	@git submodule update --init
	@export CFLAGS='-O3 -Wall -fPIC -D_GNU_SOURCE=1 -D_REENTRANT -I/usr/include/arm-linux-gnueabi -I/usr/include/SDL' \
	&& export LDFLAGS='-L/usr/lib/arm-linux-gnueabi -lSDL' \
	&& export CC=${CROSS}gcc \
	&& make -C quirc install
	@make quirc-clean

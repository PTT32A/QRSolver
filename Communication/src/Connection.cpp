#include <SSTR.h>
#include <errno.h>
#include <fcntl.h>
#include <log.h>
#include <netdb.h>
#include <poll.h>
#include <stdio.h>
#include <string>
#include "Connection.h"
#include "Message.h"

Connection::Connection() {}
Connection::~Connection() {}

bool Connection::Connect(const std::string& address, int port) {
  if (!SocketClient::Connect(address, port)) {
    std::string error = "Unable to connect to " + address + ":" + std::to_string(port);
    throw SocketException(error);
  }
  return true;
}

bool Connection::Send(const std::string& message) {
  if (!SocketClient::Send(message)) {
    AddressInfo info = GetPeerName();
    throw SocketException(SSTR("Unable to send message to " << info.address << ":" << info.port));
  }
  return true;
}

bool Connection::Poll(int timeoutMs) {
  if (!IsValid()) {
    throw std::invalid_argument("Local socket invalid");
  }

  polledFD.fd = GetFDesc();
  polledFD.events = POLLIN;
  polledFD.revents = 0;

  int resultCode = ::poll(&polledFD, 1, timeoutMs);

  if (resultCode < 0) {  // error
    std::string err = "Poll failed: " + GetErrString(errno);
    LOG(ERROR) << err;
    Disconnect();
    return false;
  }

  if (resultCode == 0) {  // timeout
    return false;
  }

  if (polledFD.revents == 0) {
    // Nothing to see here. Move along.
    return false;
  }

  if (polledFD.revents & POLLERR) {
    LOG(INFO) << "FD " << polledFD.fd << " had an error";
    Disconnect();
    throw std::runtime_error("Polled socket had an error");
  }

  if (polledFD.revents & POLLNVAL) {
    Disconnect();
    throw std::runtime_error("Invalid file descriptor");
  }

  bool retval = false;
  if (polledFD.revents & POLLIN) {
    retval = Read(polledFD.fd, &cache);
  }

  if (polledFD.revents & POLLHUP) {
    Disconnect();
    retval = retval || false;
  }

  return retval;
}

std::string Connection::Peek() { return cache.str(); }

std::string Connection::Get() {
  std::string output = cache.str();
  if (!output.empty()) {
    cache.clear();
    cache.str(std::string());
  }
  return output;
}

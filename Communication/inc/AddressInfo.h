#pragma once

#include <string>

struct AddressInfo {
  bool valid = false;
  int fDesc = -1;
  std::string address;
  int port = -1;
};

#pragma once

#include <arpa/inet.h>
#include <netinet/in.h>
#include <sys/poll.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <string>
#include "AddressInfo.h"

class Socket {
 protected:
  Socket();

 public:
  virtual ~Socket();

 public:
  static AddressInfo GetPeerName(int fDesc);
  static bool CheckValid(int desc);
  static std::string GetErrString(int err);

 public:  // Getters
  bool IsValid() const { return myFDesc != -1; }
  int GetFDesc() const { return myFDesc; }
  const sockaddr_in& GetAddr() const { return myAddress; }

  void SetMetaDataEnabled(bool enable) { metaDataEnabled = enable; }
  bool IsMetaDataEnabled() { return metaDataEnabled; }

 protected:
  void Disconnect();
  bool Configure();
  bool Configure(const sockaddr_in& address);
  bool Configure(const sockaddr_in& address, int fDesc);

  bool SetBlocking(bool blocking);
  void SetAddr(sockaddr_in address) { myAddress = address; }

  bool Read(int fDesc, std::stringstream* output);
  bool Send(int fDesc, const std::string& message);

 protected:
  static const int READ_BUFFER_SIZE = 1024;
  static const size_t META_DATA_LENGTH = sizeof(uint32_t);

 private:
  int Create();

 private:
  int myFDesc;
  sockaddr_in myAddress;
  bool metaDataEnabled = true;

 private:
  // Copy constructor disabled
  Socket(Socket&);  // NOLINT
};

#pragma once

#include <ctime>
#include <string>
#include "AddressInfo.h"

class Message {
 public:
  std::string GetContent() const { return myContent; }
  time_t GetTimestamp() const { return myTimestamp; }
  const AddressInfo& GetSenderAddress() const { return mySenderAddress; }

 public:
  Message(const std::string& content, const AddressInfo& address)
      : myContent(content), mySenderAddress(address), myTimestamp(time(0)) {}
  explicit Message(const std::string& content) : myContent(content), myTimestamp(time(0)) {}
  virtual ~Message() {}

 private:
  std::string myContent;
  AddressInfo mySenderAddress;
  time_t myTimestamp;
};

#pragma once

#include <netinet/in.h>
#include <string>
#include "AddressInfo.h"
#include "Socket.h"

class SocketClient : public Socket {
 public:
  SocketClient();
  SocketClient(int fDesc, const sockaddr_in& address);
  virtual ~SocketClient();

  bool Send(const std::string& message);
  bool Connect(const std::string& address, int port);

  AddressInfo GetPeerName();

 protected:
  int GetRemoteFDesc() { return remoteFDesc; }

 private:
  void DepleteSendBuffer(int attempts);

 private:
  int remoteFDesc = -1;
};

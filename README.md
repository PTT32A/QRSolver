[![build status](https://gitlab.com/PTT32A/QRSolver/badges/develop/build.svg)](https://gitlab.com/PTT32A/QRSolver/commits/develop)

# Cross-compile guide for ev3

## Install docker

1. run `sudo apt-get install apt-transport-https ca-certificates`
2. run `sudo apt-key adv --keyserver hkp://p80.pool.sks-keyservers.net:80 --recv-keys 58118E89F3A912897C070ADBF76221572C52609D`
3. run `echo deb https://apt.dockerproject.org/repo ubuntu-xenial main | sudo tee /etc/apt/sources.list.d/docker.list` (Assuming Ubuntu 16)
4. run `sudo apt-get update`
5. run `sudo apt-get install docker-engine`
6. run `sudo usermod -aG docker ${USER}`
7. run `sudo service docker start`

## Download ev3dev cross compiler image
1. run `sudo docker pull ev3dev/debian-jessie-cross`
2. run `sudo docker tag ev3dev/debian-jessie-cross ev3cc` //give the compiler a shorter name (optional)
3. reboot

## Cross compile
To start compiling for ev3 we have to use the downloaded docker image.
This can be done with:

`docker run --rm -it -v path/to/dir:/src -w /src ev3cc`

Let’s break down the command:
- **run** means we are running a new container.
- **--rm** indicates that we want to throw away the container when we are done. If you don’t do this, docker saves a new container from each run command, which takes up space on your hard drive.
- **-it** is two options, it means “interactive” and “tty”. This will let us use the command prompt inside of the container.
- **-v** (host-path):(container-path) lets us use a directory from our host computer inside of the container.
- **-w** (container-path) is the working directory inside of the container.
- **ev3cc** is the name of the docker image we are using.

After starting the docker terminal, run: `source docker_setup.sh`

This installs various QRSolver dependencies.

### Example

Start docker: `docker run --rm -it -v /var/git/QRSolver:/QRSolver -w /QRSolver ev3cc`
- This will map /var/git/QRSolver to /QRSolver inside the docker environment

Compile a program with (in the docker): `arm-linux-gnueabi-g++ -o hello hello.c`




Original sources:
(https://docs.docker.com/engine/installation/linux/debian/)

(http://www.ev3dev.org/docs/tutorials/using-docker-to-cross-compile/)
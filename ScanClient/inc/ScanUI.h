#pragma once
#include "ScanResult.h"
#include "Scanner.h"

class ScanUI {
 public:
  virtual ScanResult *OnScanResult(ScanResult *scanResult);
  virtual void SetScanner(Scanner *scanner);

 private:
  Scanner *scanner;
};

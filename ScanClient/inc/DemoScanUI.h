#pragma once
#include "ScanResult.h"
#include "Scanner.h"
#include "ScanUI.h"

class DemoScanUI : public ScanUI {
 public:
  ScanResult *OnScanResult(ScanResult *scanResult);
};

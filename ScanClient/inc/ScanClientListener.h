#pragma once
#include <IMessageListener.h>
#include <semaphore.h>
#include <string>
#include "Message.h"

class ScanClientListener : public IMessageListener {
 public:
  ScanClientListener();
  virtual ~ScanClientListener();
  void OnMessage(const Message& message, MessageReply* reply);
  void OnError(const std::string& error);
  void OnConnect();
  void OnDisconnect();

  bool Available();
  std::string GetMessage();
  std::string GetServerAddr();
  void SetNotifier(sem_t* sem);

 private:
  Message msg;
  bool received = false;
  sem_t* notifier;
};

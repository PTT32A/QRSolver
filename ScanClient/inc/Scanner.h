#pragma once
#include <Motioncontroller.h>
#include <SensorFactory.h>
#include <vector>
#include <Connection.h>
#include <DataHandler.h>
class Scanner {
 public:
  explicit Scanner(Connection *con);
  ~Scanner();
  void Start();
  void Pause();
  void Cancel();
  void Resume();

 private:
  std::vector<uint8_t> MoveAndMeasure(int x, int y, int interval);

  // function to find the startpoint of the QR code, which is top left.
  // std::pair<int, int> FindTopLeftCorner();
  // returns the first black pixel cooradinate
  // std::pair<int, int> FindFirstBlackValue(std::vector<uint8_t> *data);

  bool MaxDeviationRange(int coordinate, int maxCoordinate);
  // scans the QR code after starting point is found (FindTopLeftCorner)
  void ScanQr();
  // sensor from which to receive data from, initialized in constructor
  std::shared_ptr<ISensor> sensor;
  // datahandler is used to save sensor data or send it directly to server
  std::shared_ptr<DataHandler> datahandler;
  // instance to control the motors
  Motioncontroller controller;

  const int maxXPosition = 3850;
  const int maxYPosition = 2750;
  const int yStep = 25;
  const int sensorHeight = 115;
};

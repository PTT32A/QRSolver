#pragma once
#include <map>
#include <utility>
class ScanResult {
 public:
  bool IsCompleted();
  void GetData();

 private:
  std::map<std::pair<int, int>, bool> data;
};

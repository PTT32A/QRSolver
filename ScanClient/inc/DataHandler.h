#pragma once
#include <Connection.h>
#include <vector>

class DataHandler {
 public:
  explicit DataHandler(Connection *connection);
  ~DataHandler();
  void AppendDataToFile(std::vector<uint8_t> *rawData, uint32_t imageHeight = 0);
  void SendData(std::vector<uint8_t> *rawData);

 private:
  Connection *con;
};

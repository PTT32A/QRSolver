#include <ScanClientListener.h>
#include <iostream>
ScanClientListener::ScanClientListener() : msg(""), notifier(NULL) {}
ScanClientListener::~ScanClientListener() {}
void ScanClientListener::OnMessage(const Message& message, MessageReply* reply) {
  msg = message;
  received = true;
  if (notifier != NULL) {
    sem_post(notifier);
  }
}
void ScanClientListener::OnError(const std::string& error) { std::cout << error << std::endl; }
void ScanClientListener::OnConnect() { std::cout << "Connected with server" << std::endl; }
void ScanClientListener::OnDisconnect() { std::cout << "Disconnected with server" << std::endl; }
bool ScanClientListener::Available() { return received; }
std::string ScanClientListener::GetMessage() {
  received = false;
  return msg.GetContent();
}
std::string ScanClientListener::GetServerAddr() { return msg.GetSenderAddress().address; }
void ScanClientListener::SetNotifier(sem_t* sem) { notifier = sem; }

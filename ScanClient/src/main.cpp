#include <ScanClientListener.h>
#include <Scanner.h>
#include <SocketChannel.h>
#include <log.h>
#include <semaphore.h>
#include <iostream>
#include <memory>

INITIALIZE_EASYLOGGINGPP

const int port = 8500;

int main(int argc, char** argv) {
  // Demo / debug mode: just scan, without sending results
  if (argc > 1 && std::string(argv[1]) == "start") {
    Scanner scanner(NULL);
    scanner.Start();
    return 0;
  }

  sem_t semmy;
  sem_init(&semmy, 0, 0);
  std::shared_ptr<ScanClientListener> listener = std::make_shared<ScanClientListener>();
  listener->SetNotifier(&semmy);

  SocketChannel channel(port);
  channel.Start();
  channel.AddListener(listener);

  sem_wait(&semmy);  // wait for server to iniate communication

  Connection con;
  con.Connect(listener->GetServerAddr(), port);  // setup socket to send data over

  LOG(INFO) << "Server says: " << listener->GetMessage();

  Scanner scanclient(&con);
  con.Send("SCANNER READY");

  while (true) {
    // when start command received... start scanning
    if (listener->Available()) {
      if (listener->GetMessage() == "STARTSCAN") {
        con.Send("STARTSEND");
        scanclient.Start();
        con.Send("STOPSEND");
        // send done command to server
      }
      if (listener->GetMessage() == "STOPSCAN") {
        con.Send("SCANNER SHUTDOWN");
        break;
      }
    }
  }
  return 0;
}

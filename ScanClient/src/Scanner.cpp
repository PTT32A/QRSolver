#include <log.h>
#include <iterator>
#include <iostream>
#include <fstream>
#include <JpegConverter.h>
#include "Scanner.h"

Scanner::Scanner(Connection *con) {
  SensorFactory fact;
  sensor = fact.CreateSensor();
  datahandler = std::make_shared<DataHandler>(DataHandler(con));
}
Scanner::~Scanner() {}

void Scanner::Start() {
  controller.Home();
  ScanQr();
}

void Scanner::Pause() {}
void Scanner::Cancel() { controller.Home(); }
void Scanner::Resume() {}

std::vector<uint8_t> Scanner::MoveAndMeasure(int x, int y, int interval) {
  std::vector<uint8_t> XYdata;
  int prevMeasureX = 0;
  int prevMeasureY = 0;
  bool measure = true;
  while (measure) {
    // TODO(Dorian): set max deviation for x and y
    if (MaxDeviationRange(controller.GetXPos(), x) && MaxDeviationRange(controller.GetYPos(), y)) {
      measure = false;
      break;
    }
    int xpos = controller.GetXPos();
    int ypos = controller.GetYPos();
    discreteValue val = sensor->GetDiscreteValue();
    if (xpos - prevMeasureX >= interval || ypos - prevMeasureY >= interval) {
      XYdata.push_back(val);
    }
    controller.AbsolutePtP(x, y, sensorHeight);
    prevMeasureX = xpos;
    prevMeasureY = ypos;
  }
  return XYdata;
}
// was meant to find the topleft corner of a QR
/*
std::pair<int, int> Scanner::FindFirstBlackValue(std::vector<uint8_t>* data) {
  std::pair<int, int> firstBlack = std::make_pair(-1, -1);
  for (auto& val : *data) {
    if (val == 0) {
      firstBlack = std::make_pair(val, val.y);
      break;
    }
  }
  return firstBlack;
}
// was meant to find the topleft corner of a QR, not used
std::pair<int, int> Scanner::FindTopLeftCorner() {
  bool xLoop = true;
  bool yLoop = true;
  int y = maxYPosition;
  int x = maxXPosition;
  std::pair<int, int> firstBlack;
  while (yLoop) {
    while (xLoop) {
      std::vector<ScanInfo> data = MoveAndMeasure(x, y, 1);
      firstBlack = FindFirstBlackValue(&data);
      if (x == maxXPosition) {
        x = 0;
      } else {
        x = maxXPosition;
      }
      if (firstBlack == std::make_pair(-1, -1)) {
        break;
      } else {
        xLoop = false;
        yLoop = false;
      }
    }
    y -= yStep;
  }
  MoveAndMeasure(firstBlack.first, firstBlack.second, 1);  // move to correct position for up scanning
  std::vector<ScanInfo> data = MoveAndMeasure(firstBlack.first, maxYPosition, 1);
  std::pair<int, int> secondBlack = FindFirstBlackValue(&data);  // move up until max y;
  std::pair<int, int> finalReturnValue = firstBlack;
  if (secondBlack != std::make_pair(-1, -1)) {
    finalReturnValue.second = secondBlack.second;  // only gets set when black
                                                   // pixels are above first
                                                   // black pixel
  }
  // move back to last black pixel - start sequence finished
  MoveAndMeasure(finalReturnValue.first, finalReturnValue.second, 1);
  return finalReturnValue;
}
*/
void Scanner::ScanQr() {
  controller.AbsolutePtP(0, maxYPosition, 0);             // move Z axis to 0
  controller.AbsolutePtP(0, maxYPosition, sensorHeight);  // move Z axis down

  const int amountOfStepsToMoveDown = -100;
  const int minYposition = 500;
  const int minXposition = 400;

  int x = controller.GetXPos();
  int y = controller.GetYPos();
  uint32_t qrHeight = 0;  // doing nothing with it yet
  LOG(INFO) << "Going to scan QR now";
  // scanner starts top left Y=MaxValue, X=MinValue
  // scanner ends bottom right Y->MinValue X=MaxValue
  while (y > minYposition && x < maxXPosition) {
    // x, y, z
    // move to X->Maxval, Y-> current position
    x = controller.GetXPos();
    y = controller.GetYPos();  // update y coordinate because it changed
    std::vector<uint8_t> YdataRight = MoveAndMeasure(maxXPosition, y, 1);
    LOG(INFO) << "Datasize: " << YdataRight.size();
    // datahandler->SendData(&YdataRight);

    controller.RelativePtP(0, amountOfStepsToMoveDown, 0);  // move down, no need to scan
    qrHeight++;
    y = controller.GetYPos();  // update y coordinate because it changed
    LOG(INFO) << "y = " << y;

    std::vector<uint8_t> YdataLeft = MoveAndMeasure(minXposition, y, 1);
    std::reverse(YdataLeft.begin(), YdataLeft.end());
    LOG(INFO) << "Datasize: " << YdataLeft.size();
    // datahandler->SendData(&YdataLeft);

    controller.RelativePtP(0, amountOfStepsToMoveDown, 0);  // move down, no need to scan
    qrHeight++;

    LOG(INFO) << "x = " << y;
  }

  /*
  how the scan algorithm works
  ----->1
       |
  <----V2
  |
  V---->3

  QR library wants this:
  ---->
  ---->
  ---->

  so we have to reverse the even number rows
  */
}

bool Scanner::MaxDeviationRange(int currentCoordinate, int maxCoordinate) {
  // coordinate may deviate with 10
  if (currentCoordinate >= (maxCoordinate - 10) && currentCoordinate <= (maxCoordinate + 10)) {
    return true;
  }
  return false;
}

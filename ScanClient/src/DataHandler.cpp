#include <DataHandler.h>
#include <DataString.h>

DataHandler::DataHandler(Connection *connection) : con(connection) {}
DataHandler::~DataHandler() {}
void DataHandler::AppendDataToFile(std::vector<uint8_t> *rawData, uint32_t imageHeight) {
  /*
  std::ofstream sensorData;
  sensorData.open("sensorData.bin", std::ios::out | std::ios::binary);
  try {
    if (imageHeight > 0) {
      // if imgHeight is not default val, write it to file
      sensorData << imageHeight;
    } else {
      std::ostream_iterator<uint8_t> output_iterator(sensorData);
      std::copy(rawData.begin(), rawData.end(), output_iterator);
    }
  }*/
}
void DataHandler::SendData(std::vector<uint8_t> *rawData) {
  DataString converter;
  std::vector<uint8_t> &dataRef = *rawData;
  if (con != NULL) {
    con->Send(converter.ToString(&dataRef[0], rawData->size()));
  }
}

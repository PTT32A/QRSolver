#Releasenotes

##Functionaliteit

- Server Kan plotter aansturen
- QR-code afbeeldingen kunnen worden omgezet naar text
- Scanner kan scanbewegingen maken
- plotter kan text plotten
- volledige socket communicatie tussen scanner,plotter, controlcenter
- GPIO afhandeling voor programma reset/opstart

##Known issues
- Scanner stuurt geen data naar server
- fout afhandeling in socket communicatie niet volledig